<?php
/*
  Template Name: Galleries
*/
?>
<?php get_header(); ?>

<div class="main-wrapper full-width clearfix" id="main-wrapper">
        <div class="main region grid16-16" id="main">
          <div class="main-inner inner clearfix" id="main-inner">
            
            <!-- main group: width = grid_width - sidebar_first_width -->
            <div class="main-group region nested grid16-16" id="main-group">
              <div class="main-group-inner inner" id="main-group-inner">
                
                <div class="main-content region nested" id="main-content">
                  <div class="main-content-inner inner" id="main-content-inner">
                    <!-- content group: width = grid_width - sidebar_first_width - sidebar_second_width -->
                    <div class="content-group region nested grid16-16" id="content-group">
                      <div class="content-group-inner inner" id="content-group-inner">
                                                
                        <div class="content-region region nested" id="content-region">
                          <div class="content-region-inner inner" id="content-region-inner">
                            <a id="main-content-area"></a>
                                                                                                                                            <h1 class="title gutter">Galleries</h1>
                                                                                                                                              
<!-- content region -->
  <div class="region region-content content nested grid16-16" id="content">
    <div class="content-inner inner" id="content-inner">
      <div class="block block-system first last odd" id="block-system-main">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="view view-galleries view-id-galleries view-display-id-page view-dom-id-25a3d55dc1d304568e427979f25807ad">
        
  
  
      <div class="view-content">
        <h3>2016</h3>

      <?php

      // check if the repeater field has rows of data
      if( have_rows('gallery_2016') ):
        // loop through the rows of data
          while ( have_rows('gallery_2016') ) : the_row();

              // display a sub field value
              //the_sub_field('sub_field_name'); ?>
              <div class="views-row views-row-1 views-row-odd views-row-first">
                  
              <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="<?php the_sub_field('link'); ?>"><img width="140" height="100" title="F" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></a></div>  </div>  
              <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-mulwala"><?php the_sub_field('title'); ?></a></span>  </div>  </div>

      <?php
          endwhile;
      else :
          // no rows found
      endif;
      ?>

<!--
  <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-1-mulwala"><img width="140" height="100" title="F" src="<?php echo get_template_directory_uri(); ?>/images/_BKA1157.jpg?itok=stnRaYSX" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-mulwala">Round 1 | Mulwala</a></span>  </div>  </div>



  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-3-grafton-0"><img width="140" height="100" title="F" src="<?php echo get_template_directory_uri(); ?>/images/_BKA6835.jpg?itok=ZqOg9N99" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-3-grafton-0">Round 3 | Grafton</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-4-port-macquarie-0"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/_BKA0067_1.jpg?itok=A1rnIjFU" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-4-port-macquarie-0">Round 4 | Port Macquarie</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even views-row-last">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-4-port-macquarie-1"><img width="140" height="100" title="f" src="<?php echo get_template_directory_uri(); ?>/images/_BKA7079.jpg?itok=KwEIUaAX" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-4-port-macquarie-1">Round 4 | Port Macquarie</a></span>  </div>  </div>-->


  <h3>2014 / 2015</h3>



      <?php

      // check if the repeater field has rows of data
      if( have_rows('gallery_2014-2015') ):
        // loop through the rows of data
          while ( have_rows('gallery_2014-2015') ) : the_row();

              // display a sub field value
              //the_sub_field('sub_field_name'); ?>
              <div class="views-row views-row-1 views-row-odd views-row-first">
                  
              <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="<?php the_sub_field('link'); ?>"><img width="140" height="100" title="F" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></a></div>  </div>  
              <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-mulwala"><?php the_sub_field('title'); ?></a></span>  </div>  </div>

      <?php
          endwhile;
      else :
          // no rows found
      endif;
      ?>



<!--
  <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-1-wagga"><img width="140" height="100" title="F" src="<?php echo get_template_directory_uri(); ?>/images/BKA_3907.jpg?itok=36gU2URK" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-wagga">Round 1 | Wagga</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-2-griffith"><img width="140" height="100" title="F" src="<?php echo get_template_directory_uri(); ?>/images/DJI00005.jpg?itok=1TWRWtGR" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-2-griffith">Round 2 | Griffith</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-3-mulwala-0"><img width="140" height="100" title="F" src="<?php echo get_template_directory_uri(); ?>/images/BKA_7740.jpg?itok=1CdFdtoD" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-3-mulwala-0">Round 3 | Mulwala</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-4-grafton"><img width="140" height="100" title="f" src="<?php echo get_template_directory_uri(); ?>/images/_BKA3620.jpg?itok=oi1lqZ3E" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-4-grafton">Round 4 | Grafton</a></span>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd views-row-last">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-5-port-macquarie-0"><img width="140" height="100" title="f" src="<?php echo get_template_directory_uri(); ?>/images/_BKA0477.jpg?itok=QIY-3HMO" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-5-port-macquarie-0">Round 5 | Port Macquarie </a></span>  </div>  </div>
-->

  <h3>2013-2014</h3>

      <?php

      // check if the repeater field has rows of data
      if( have_rows('gallery_2013-2014') ):
        // loop through the rows of data
          while ( have_rows('gallery_2013-2014') ) : the_row();

              // display a sub field value
              //the_sub_field('sub_field_name'); ?>
              <div class="views-row views-row-1 views-row-odd views-row-first">
                  
              <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="<?php the_sub_field('link'); ?>"><img width="140" height="100" title="F" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></a></div>  </div>  
              <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-mulwala"><?php the_sub_field('title'); ?></a></span>  </div>  </div>

      <?php
          endwhile;
      else :
          // no rows found
      endif;
      ?>

  <!--
  <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-1-bundaberg-1"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/Bundy%20AFPGP%202013%20002_1.JPG?itok=Z1Z3K_bd" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-bundaberg-1">Round 1 | Bundaberg</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-2-wagga-0"><img width="140" height="100" title="F" src="<?php echo get_template_directory_uri(); ?>/images/1.jpg?itok=k2-6CAA7" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-2-wagga-0">Round 2 | Wagga</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-3-mulwala"><img width="140" height="100" title="F" src="<?php echo get_template_directory_uri(); ?>/images/DJI00009.jpg?itok=GRcy5MYa" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-3-mulwala">Round 3 | Mulwala</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-4-penrith-0"><img width="140" height="100" title="F" src="<?php echo get_template_directory_uri(); ?>/images/_BKA9014.jpg?itok=ZMXwnEyF" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-4-penrith-0">Round 4 | Penrith</a></span>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd views-row-last">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-5-port-macquarie"><img width="140" height="100" title="F" src="<?php echo get_template_directory_uri(); ?>/images/DJI00013_1.jpg?itok=VhFePUkP" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-5-port-macquarie">Round 5 | Port Macquarie </a></span>  </div>  </div>
-->


  <h3>2012-2013</h3>

      <?php

      // check if the repeater field has rows of data
      if( have_rows('gallery_2012-2013') ):
        // loop through the rows of data
          while ( have_rows('gallery_2012-2013') ) : the_row();

              // display a sub field value
              //the_sub_field('sub_field_name'); ?>
              <div class="views-row views-row-1 views-row-odd views-row-first">
                  
              <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="<?php the_sub_field('link'); ?>"><img width="140" height="100" title="F" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></a></div>  </div>  
              <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-mulwala"><?php the_sub_field('title'); ?></a></span>  </div>  </div>

      <?php
          endwhile;
      else :
          // no rows found
      endif;
      ?>

  <!--
  <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-1-bundaberg"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%20BundyPB2-11-2012%2022600x400.jpg?itok=AX2Y-DNu" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-bundaberg">Round 1 | Bundaberg</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-2-wagga"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%20mPowerBoatsB23-2-2013%2034.jpg?itok=sUIeCuLQ" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-2-wagga">Round 2 | Wagga</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-3-grafton"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%20DSC03603.jpg?itok=XfcXtTtI" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-3-grafton">Round 3 | Grafton</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-4-taree"><img width="140" height="100" title="F" src="<?php echo get_template_directory_uri(); ?>/images/1%20AFPGP%20Taree%202013%20024.JPG?itok=LBst6SsZ" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-4-taree">Round 4 | Taree</a></span>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd views-row-last">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-5-port"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/AFPGP%20Port%20Macquarie%20Saturday%20001.JPG?itok=kB8vpR8B" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-5-port">Round 5 | Port</a></span>  </div>  </div>
-->

  <h3>2011-2012</h3>

      <?php

      // check if the repeater field has rows of data
      if( have_rows('gallery_2011-2012') ):
        // loop through the rows of data
          while ( have_rows('gallery_2011-2012') ) : the_row();

              // display a sub field value
              //the_sub_field('sub_field_name'); ?>
              <div class="views-row views-row-1 views-row-odd views-row-first">
                  
              <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="<?php the_sub_field('link'); ?>"><img width="140" height="100" title="F" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></a></div>  </div>  
              <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-mulwala"><?php the_sub_field('title'); ?></a></span>  </div>  </div>

      <?php
          endwhile;
      else :
          // no rows found
      endif;
      ?>


  <!--
  <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-1-taree-1"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%20F2%20mPowerBoats18-2-2012%20438.jpg?itok=fhzpvQNz" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-taree-1">Round 1 | Taree</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-2-grafton-1"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%206%20FormulaPB22-4-2012%20213Sundayresized600x400.jpg?itok=dM3U48h3" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-2-grafton-1">Round 2 | Grafton</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-3-wagga"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%20PB17-6-2012%2020Wagga2012%20400x600.jpg?itok=VxuXNnE-" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-3-wagga">Round 3 | Wagga</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even views-row-last">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-4-port-macquarie"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%20GEN%20FormulaPB29-7-2012%201657Port2012%20600x400.jpg?itok=vb4BytqT" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-4-port-macquarie">Round 4 | Port Macquarie</a></span>  </div>  </div> -->



  <h3>2010-2011</h3>

      <?php

      // check if the repeater field has rows of data
      if( have_rows('gallery_2010-2011') ):
        // loop through the rows of data
          while ( have_rows('gallery_2010-2011') ) : the_row();

              // display a sub field value
              //the_sub_field('sub_field_name'); ?>
              <div class="views-row views-row-1 views-row-odd views-row-first">
                  
              <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="<?php the_sub_field('link'); ?>"><img width="140" height="100" title="F" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></a></div>  </div>  
              <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-mulwala"><?php the_sub_field('title'); ?></a></span>  </div>  </div>

      <?php
          endwhile;
      else :
          // no rows found
      endif;
      ?>


  <!--
  <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-1-taree"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%200020wi.jpg?itok=GCIo6bKF" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-taree">Round 1 | Taree</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-2-grafton-0"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%200046wi.jpg?itok=7b43wLot" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-2-grafton-0">Round 2 | Grafton</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-3-port-macquarie"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%20DSC_6903.jpg?itok=0Z-qsRi5" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-3-port-macquarie">Round 3 | Port Macquarie</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even views-row-last">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-4-penrith"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%20mPowerBoats27-8-2011%2072Penrith211%20400x600.jpg?itok=8kKu9PQK" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-4-penrith">Round 4 | Penrith</a></span>  </div>  </div> -->
  <h3>2009-2010</h3>

      <?php

      // check if the repeater field has rows of data
      if( have_rows('gallery_2009-2010') ):
        // loop through the rows of data
          while ( have_rows('gallery_2009-2010') ) : the_row();

              // display a sub field value
              //the_sub_field('sub_field_name'); ?>
              <div class="views-row views-row-1 views-row-odd views-row-first">
                  
              <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="<?php the_sub_field('link'); ?>"><img width="140" height="100" title="F" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></a></div>  </div>  
              <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-mulwala"><?php the_sub_field('title'); ?></a></span>  </div>  </div>

      <?php
          endwhile;
      else :
          // no rows found
      endif;
      ?>

  <!--
  <div class="views-row views-row-1 views-row-odd views-row-first">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-1-taree-0"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%20swfimage_gallerygalleryimages-TAREE_1_1.jpg?itok=bitAVW4_" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-1-taree-0">Round 1 | Taree</a></span>  </div>  </div>
  <div class="views-row views-row-2 views-row-even">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-2-grafton"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%200049wi.jpg?itok=LHG00Sj0" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-2-grafton">Round 2 | Grafton</a></span>  </div>  </div>
  <div class="views-row views-row-3 views-row-odd">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-3-geelong"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%200716wi.jpg?itok=3qcaSnsO" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-3-geelong">Round 3 | Geelong</a></span>  </div>  </div>
  <div class="views-row views-row-4 views-row-even">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-4-port"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%200018wi.jpg?itok=UbyAb_Ja" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-4-port">Round 4 | Port</a></span>  </div>  </div>
  <div class="views-row views-row-5 views-row-odd views-row-last">
      
  <div class="views-field views-field-field-gallery-images">        <div class="field-content"><a href="/gallery/round-5-penrith"><img width="140" height="100" src="<?php echo get_template_directory_uri(); ?>/images/1%200002wi.jpg?itok=gaAe9ETM" typeof="foaf:Image"></a></div>  </div>  
  <div class="views-field views-field-title">        <span class="field-content"><a href="/gallery/round-5-penrith">Round 5 | Penrith</a></span>  </div>  </div>
-->

    </div>
  
  
  
  
  
  
</div>    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /content-inner -->
  </div><!-- /content -->
                                                      </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->
                                      </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div>
            
<?php get_footer(); ?>