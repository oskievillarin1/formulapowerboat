<?php
/*
  Template Name: Sponsorship
*/
?>
<?php get_header(); ?>

<div class="main-wrapper full-width clearfix" id="main-wrapper">
        <div class="main region grid16-16" id="main">
          <div class="main-inner inner clearfix" id="main-inner">
            
            <!-- main group: width = grid_width - sidebar_first_width -->
            <div class="main-group region nested grid16-16" id="main-group">
              <div class="main-group-inner inner" id="main-group-inner">
                
                <div class="main-content region nested" id="main-content">
                  <div class="main-content-inner inner" id="main-content-inner">
                    <!-- content group: width = grid_width - sidebar_first_width - sidebar_second_width -->
                    <div class="content-group region nested grid16-16" id="content-group">
                      <div class="content-group-inner inner" id="content-group-inner">
                                                
                        <div class="content-region region nested" id="content-region">
                          <div class="content-region-inner inner" id="content-region-inner">
                            <a id="main-content-area"></a>
                                                                                                                                            <h1 class="title gutter"><div data-edit-id="node/13/title/und/full"><div class="field-item">Sponsorship</div></div></h1>
                                                                                                                                              
<!-- content region -->
  <div class="region region-content content nested grid16-16" id="content">
    <div class="content-inner inner" id="content-inner">
      <div class="block block-system first last odd" id="block-system-main">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="ds-2col node node-page odd full-node view-mode-full clearfix" typeof="foaf:Document" about="/sponsorship">

  
  <div class="group-left">
    <div data-edit-id="node/13/body/und/full" class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even">

        <?php while ( have_posts() ) : the_post(); ?>       

            <?php the_content(); ?>

        <?php endwhile; ?>

<!--
      <p>The AUSTRALIAN FORMULA POWERBOAT GRAND PRIX provides in itself an aura of elite competition, speed and excellence. FORMULA Powerboats are the ultimate in outboard circuit racing and provide all of the elements a motor sporting enthusiast would desire or expect from this premier form of competition. It is however still targeted at the family audience and with the introduction of our new Formula Futures (Junior Class), the family is well catered for during each event.<br><br>
  For those not familiar with the sport the initial sight of a Formula Powerboat can be awe-inspiring. The series has obtained strong support from local and national companies along with a growing support from local and state governments. Due to the larger audiences, the Grand Prix is also a tourist attraction in areas where these events are held.&nbsp;<br><br>
  There are many marketing partnership sponsor packages available, most with the ability to be tailored to each company's marketing objectives, key brand values and specific product exposure. All key sponsorship packages include media promotional campaigns and can include (but not limited to) any or all of the following print media; newspaper, display posters, event programmes, pre and post race articles in national boating magazines, radio, website and site promotions (media launches, shopping centre displays and event displays).<br><br>
  Sponsorship Packages are designed to cater from a Co-Sponsor right through to NAMING RIGHTS for each event. Sponsorship is also inexpensive and can start from as little as $500 (plus GST).<br><br>
  To expand further on what official products and services we believe have a natural synergy with our sport please refer to the list below:</p>

<ul><li>vehicles / car hire companies</li>
  <li>retail products</li>
  <li>telecommunications</li>
  <li>clothing / merchandise</li>
  <li>engineering firms</li>
  <li>tyre / fuel suppliers</li>
  <li>jet ski dealers / associated companies</li>
  <li>accommodation chains</li>
  <li>finance / development firms</li>
  <li>FMCG / hospitality suppliers</li>
</ul><p>Become a Grand Prix Sponsor and enjoy maximum exposure of your business over a number of thrilling rounds, set in key tourist areas along the east coast of Australia. Each round offers something unique for corporate partners to promote their products and services whilst taking advantage of picturesque scenery, magnificent water backdrops and the individual.</p>

<p>For more information, please contact our Sponsorship Co-ordinator Gavin Simmons&nbsp;on 0421 731 739</p>
-->

</div></div></div>  </div>

  <div class="group-right">
    <div data-edit-id="node/13/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items">

      <?php

      // check if the repeater field has rows of data
      if( have_rows('side_images') ):

        // loop through the rows of data
          while ( have_rows('side_images') ) : the_row();

              // display a sub field value
              //the_sub_field('sub_field_name'); ?>
            <div class="field-item even about_side_img"><img alt="image" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></div>

      <?php

          endwhile;

      else :

          // no rows found

      endif;

      ?>


      <!--
      <div class="field-item even"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Sponsorship%201.jpg?itok=O7MRDEKn" typeof="foaf:Image"></div>

      <div class="field-item odd"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Sponsorship%202.jpg?itok=W01L948l" typeof="foaf:Image"></div>

      <div class="field-item even"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Sponsorship%203.jpg?itok=FTjOOMD1" typeof="foaf:Image"></div><div class="field-item odd"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Sponsorship%204.jpg?itok=_RVWACM-" typeof="foaf:Image"></div><div class="field-item even"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Sponsorship%206.jpg?itok=-co5WXPo" typeof="foaf:Image"></div>
    -->

    </div></div>  </div>

</div>

    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /content-inner -->
  </div><!-- /content -->
                                                      </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->
                                      </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div>
            
<?php get_footer(); ?>