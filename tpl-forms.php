<?php
/*
  Template Name: Forms
*/
?>
<?php get_header(); ?>

<div class="main-wrapper full-width clearfix" id="main-wrapper">
        <div class="main region grid16-16" id="main">
          <div class="main-inner inner clearfix" id="main-inner">
            
            <!-- main group: width = grid_width - sidebar_first_width -->
            <div class="main-group region nested grid16-16" id="main-group">
              <div class="main-group-inner inner" id="main-group-inner">
                
                <div class="main-content region nested" id="main-content">
                  <div class="main-content-inner inner" id="main-content-inner">
                    <!-- content group: width = grid_width - sidebar_first_width - sidebar_second_width -->
                    <div class="content-group region nested grid16-16" id="content-group">
                      <div class="content-group-inner inner" id="content-group-inner">
                                                
                        <div class="content-region region nested" id="content-region">
                          <div class="content-region-inner inner" id="content-region-inner">
                            <a id="main-content-area"></a>
                                                                                                                                            <h1 class="title gutter"><div data-edit-id="node/92/title/und/full"><div class="field-item">Forms</div></div></h1>
                                                                                                                                              
<!-- content region -->
  <div class="region region-content content nested grid16-16" id="content">
    <div class="content-inner inner" id="content-inner">
      <div class="block block-system first last odd" id="block-system-main">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="ds-2col node node-page odd full-node view-mode-full clearfix" typeof="foaf:Document" about="/forms">

  
  <div class="group-left">
    <div data-edit-id="node/92/body/und/full" class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even"><style type="text/css">
&lt;!--/*--&gt;&lt;![CDATA[/* &gt;&lt;!--*/
.content a { color: blue; }

/*--&gt;&lt;!]]&gt;*/
</style>



        <?php /* Start the Loop */ ?>
        <?php while ( have_posts() ) : the_post(); ?>       

            <?php the_content(); ?>

        <?php endwhile; ?>

<!--
<div class="info-box">
  <h3>MEMBERSHIP FORMS</h3>

  <ul><li><a href="/sites/default/files/images/AFPGP%20Levels%20of%20Membership%28UPDATE%29_FINAL.pdf">Levels of Membership&nbsp;</a></li>
    <li><a href="/sites/default/files/pdf/MembershipForms/2016-Gold-Membership.pdf">2016&nbsp;GOLD&nbsp;Membership</a>&nbsp;(Licensed Driver)</li>
    <li><a href="/sites/default/files/pdf/MembershipForms/2016-Silver-Membership.pdf">2016&nbsp;SILVER&nbsp;Membership</a>&nbsp;(Boat Owners, Team Crew Chiefs,&nbsp;Committee Members &amp; FF Guardians)</li>
    <li><a href="/sites/default/files/pdf/MembershipForms/2016-Bronze-Membership.pdf">&#8203;2016&nbsp;BRONZE&nbsp;Membership</a>&nbsp;(Associate Members)</li>
    <li><a href="/sites/default/files/pdf/MembershipForms/2016-FF-Membership.pdf">2016 Formula Future Membership</a></li>
  </ul><h3>ENTRY FORMS:</h3>

  <ul><li><a href="/sites/default/files/pdf/EntryForms/Updated%20AFPGP%20Senior%20Entry%20Form%20port.docx">Round 4&nbsp;| PORT MACQUARIE&nbsp;- Driver Entry Form</a></li>
    <li><a href="/sites/default/files/pdf/EntryForms/Updated%20AFPGP%20Form%20Future%20Entry%20Form%20port.docx">Round 4&nbsp;| PORT MACQUARIE- Formula Future Entry Form</a></li>
  </ul><h3>EVENT NOTES:</h3>

  <ul><li><a href="/sites/default/files/pdf/EventNotes/Event%20Notes%20-%20Port%202016.docx">Round 4&nbsp;|&nbsp;PORT MACQUARIE</a></li>
  </ul><h3>OTHER:</h3>

  <ul><li><a href="/sites/default/files/pdf/AFPGP%202013%20-%202014%20Sponsorship%20Proposal%20-%20P.pdf">&#8203;</a><a href="/sites/default/files/pdf/AFPGP%202016%20Sponsorship%20Proposal%20Port%20V1%20small%20file%20size.pdf">AFPGP&nbsp;Sponsorship Proposal</a>&nbsp;</li>
    <li><a href="/sites/default/files/pdf/2015-16%20Licence%20Payment%20Methods.pdf">2015 / 2016 License Payment Methods</a></li>
    <li><a href="/sites/default/files/pdf/2015-16%20Licence%20Prices%20%26%20Information.pdf">2015 / 2016 Prices &amp; Information</a></li>
    <li><a href="/sites/default/files/pdf/2015-2016%20Race%20Calendar%20and%20NSW%20Executive.pdf">2015 / 2016 Race Calander &amp; NSW Executives</a></li>
    <li><a href="/sites/default/files/pdf/APBA%20Medical%20Form%202015-2016.docx">APBA Medical Form</a></li>
    <li><a href="/sites/default/files/pdf/Boat%20Owner%20Form%202015-2016.pdf">2015 / 2016 Boat Owner Form</a></li>
    <li><a href="/sites/default/files/pdf/Formula%20Future%20Licence%20%26%20Medical%202014-2015.pdf">2015 / 2016 Formula Future License &amp; Medical</a></li>
    <li><a href="/sites/default/files/pdf/Licence%20Form%202015-2016.pdf">2015 / 2016 License Form</a></li>
  </ul></div>

<div class="info-box">
  <h1>RULES</h1>

  <ul><li><a href="/sites/default/files/pdf/ClubRules/AFPGP%20Constitution%20-%20Adopted%202-11-13.pdf">Club Constitution</a>&nbsp;&nbsp; (effective 29 Nov 13)</li>
    <li><a href="/sites/default/files/images/2014%20AFPGP%20Series%20Race%20Rules%20v3_2014_11_12_FINAL%20DRAFT.pdf">AFPGP Series Racing Rules</a> (adopted 12 Nov 2014)</li>
    <li><a href="http://www.ausapba.com.au/download_index.htm">APBA Rule Book 2014</a></li>
  </ul><p><span style="line-height: 1.6em;">As an Incorporated Association, we are also governed by the following legislation:</span></p>

  <ul><li><a href="/sites/default/files/pdf/ClubRules/Associations%20Incorporation%20Act%202009.pdf">Associations Incorporation Act 2009</a></li>
    <li><a href="/sites/default/files/pdf/ClubRules/Associations%20Incorporation%20Regulation%202010.pdf">Associations Incorporation Regulation 2010</a></li>
  </ul></div>
</div>
-->

</div></div>  </div>

  <div class="group-right">
    <div data-edit-id="node/92/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items">


<?php

// check if the repeater field has rows of data
if( have_rows('forms_side_images') ):

  // loop through the rows of data
    while ( have_rows('forms_side_images') ) : the_row();

        // display a sub field value
        //the_sub_field('sub_field_name'); ?>
      <div class="field-item even about_side_img"><img alt="image" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></div>

<?php

    endwhile;

else :

    // no rows found

endif;

?>
<!--
      <div class="field-item even"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Forms%20Rules%201.jpg?itok=MTw3Uf8s" typeof="foaf:Image"></div><div class="field-item odd"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Forms%20Rules%202.jpg?itok=MUq6OFmC" typeof="foaf:Image"></div><div class="field-item even"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Forms%20Rules%204.jpg?itok=LDZiJqk3" typeof="foaf:Image"></div><div class="field-item odd"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Forms%20Rules%205.jpg?itok=lpHscg9G" typeof="foaf:Image"></div><div class="field-item even"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/contact%20us%2012.jpg?itok=rvHT_CJL" typeof="foaf:Image"></div>
-->

    </div></div>  </div>

</div>

    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /content-inner -->
  </div><!-- /content -->
                                                      </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->
                                      </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div>
            
<?php get_footer(); ?>