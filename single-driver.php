<?php get_header(); ?>


<div class="main-wrapper full-width clearfix" id="main-wrapper">
        <div class="main region grid16-16" id="main">
          <div class="main-inner inner clearfix" id="main-inner">
            
            <!-- main group: width = grid_width - sidebar_first_width -->
            <div class="main-group region nested grid16-16" id="main-group">
              <div class="main-group-inner inner" id="main-group-inner">
                
                <div class="main-content region nested" id="main-content">
                  <div class="main-content-inner inner" id="main-content-inner">
                    <!-- content group: width = grid_width - sidebar_first_width - sidebar_second_width -->
                    <div class="content-group region nested grid16-16" id="content-group">
                      <div class="content-group-inner inner" id="content-group-inner">
                                                
                        <div class="content-region region nested" id="content-region">
                          <div class="content-region-inner inner" id="content-region-inner">
                            <a id="main-content-area"></a>
                                                                                                                                                                                                                                  
<!-- content region -->
  <div class="region region-content content nested grid16-16" id="content">
    <div class="content-inner inner" id="content-inner">
      <div class="block block-system first last odd" id="block-system-main">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="view view-drivers view-id-drivers view-display-id-page view-dom-id-fa97e0b7267bd7c2e32560e53b20ebef">
        
  
  
      <div class="view-content">


        <?php /* Start the Loop */ ?>
        <?php while ( have_posts() ) : the_post(); ?>     


        <h3><?php the_title(); ?></h3>


      <?php

      // check if the repeater field has rows of data
      if( have_rows('driver_details') ):
        // loop through the rows of data
          while ( have_rows('driver_details') ) : the_row();

              // display a sub field value
              //the_sub_field('sub_field_name'); ?>

        <div class="views-row views-row-1 views-row-odd views-row-first">
          <div class="ds-2col node node-drivers node-teaser odd  view-mode-teaser clearfix" typeof="sioc:Item foaf:Document" about="/drivers/david-minton-speed-and-leisure-43">
        <div class="group-left">
          <div data-edit-id="node/689/field_photos/und/teaser" class="field field-name-field-photos field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img width="249" height="150" alt="" src="<?php the_sub_field('boat_image'); ?>" typeof="foaf:Image"></div></div></div><div class="view view--eva-extra-image-on-driver-profile view-id-_eva_extra_image_on_driver_profile view-display-id-entity_view_1 view-dom-id-f7e1d4103d408f92a7a775ea103dd733">
            <div class="view-content">
                <ul>          <li class="">  
                <img width="120" height="150" alt="" src="<?php the_sub_field('driver_image'); ?>" typeof="foaf:Image">  </li>
            </ul>    </div>
      </div>   </div>
        <div class="group-right">

          
          <div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/664/title/und/teaser"><div class="field-item"><?php the_sub_field('driver_name'); ?></div></div></h2></div></div></div>

          <?php the_sub_field('driver_details_inside'); ?>

          <!--
          <div data-edit-id="node/664/field_sponsors/und/teaser" class="field field-name-field-sponsors field-type-text-long field-label-inline clearfix"><div class="field-label">Sponsors:&nbsp;</div><div class="field-items"><div class="field-item even">Speed &amp; Leisure Marine, Premier Fasteners </div></div></div>

          <div data-edit-id="node/664/field_hull_manufacturer/und/teaser" class="field field-name-field-hull-manufacturer field-type-text field-label-inline clearfix"><div class="field-label">Hull Manufacturer:&nbsp;</div><div class="field-items"><div class="field-item even">Mcconaghy</div></div></div>

          <div data-edit-id="node/664/field_home_town/und/teaser" class="field field-name-field-home-town field-type-text field-label-inline clearfix"><div class="field-label">Home Town:&nbsp;</div><div class="field-items"><div class="field-item even">Caringbah, NSW</div></div></div>

          <div data-edit-id="node/664/field_occupation/und/teaser" class="field field-name-field-occupation field-type-text field-label-inline clearfix"><div class="field-label">Occupation:&nbsp;</div><div class="field-items"><div class="field-item even">Marine Mechanic</div></div></div>  -->

      </div>
      </div>
        </div>



      <?php
          endwhile;
      else :
          // no rows found
      endif;
      ?>              






       <?php endwhile; ?>        



<!--
  <div class="views-row views-row-1 views-row-odd views-row-first">
    <div class="ds-2col node node-drivers node-teaser odd  view-mode-teaser clearfix" typeof="sioc:Item foaf:Document" about="/drivers/david-minton-speed-and-leisure-43">
  <div class="group-left">
    <div class="view view--eva-extra-image-on-driver-profile view-id-_eva_extra_image_on_driver_profile view-display-id-entity_view_1 view-dom-id-f39944128f7609f1567b9cbc9a23e772">
      <div class="view-content">
          <ul>          <li class="">  
            </li>
      </ul>    </div>
</div>   </div>
  <div class="group-right">
    <div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/664/title/und/teaser"><div class="field-item"> DAVID MINTON : Speed and Leisure #43</div></div></h2></div></div></div><div data-edit-id="node/664/field_sponsors/und/teaser" class="field field-name-field-sponsors field-type-text-long field-label-inline clearfix"><div class="field-label">Sponsors:&nbsp;</div><div class="field-items"><div class="field-item even">Speed &amp; Leisure Marine, Premier Fasteners </div></div></div><div data-edit-id="node/664/field_hull_manufacturer/und/teaser" class="field field-name-field-hull-manufacturer field-type-text field-label-inline clearfix"><div class="field-label">Hull Manufacturer:&nbsp;</div><div class="field-items"><div class="field-item even">Mcconaghy</div></div></div><div data-edit-id="node/664/field_home_town/und/teaser" class="field field-name-field-home-town field-type-text field-label-inline clearfix"><div class="field-label">Home Town:&nbsp;</div><div class="field-items"><div class="field-item even">Caringbah, NSW</div></div></div><div data-edit-id="node/664/field_occupation/und/teaser" class="field field-name-field-occupation field-type-text field-label-inline clearfix"><div class="field-label">Occupation:&nbsp;</div><div class="field-items"><div class="field-item even">Marine Mechanic</div></div></div>  </div>
</div>
  </div>



  <div class="views-row views-row-2 views-row-even">
    <div class="ds-2col node node-drivers node-teaser even  view-mode-teaser clearfix" typeof="sioc:Item foaf:Document" about="/drivers/luke-sharp-nz-56">

  
  <div class="group-left">
    <div data-edit-id="node/689/field_photos/und/teaser" class="field field-name-field-photos field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img width="249" height="150" alt="" src="http://www.formulapowerboats.com.au/sites/default/files/styles/profile_boat/public/160116_850%20%281280x853%29.jpg?itok=sEA3NF8c" typeof="foaf:Image"></div></div></div><div class="view view--eva-extra-image-on-driver-profile view-id-_eva_extra_image_on_driver_profile view-display-id-entity_view_1 view-dom-id-f7e1d4103d408f92a7a775ea103dd733">
      
  
  
      <div class="view-content">
          <ul>          <li class="">  
          <img width="120" height="150" alt="" src="http://www.formulapowerboats.com.au/sites/default/files/styles/profile_face/public/18-01-14%201499%20%28853x1280%29.jpg?itok=Dov0j9a8" typeof="foaf:Image">  </li>
      </ul>    </div>
  
  
  
  
  
  
</div>   </div>

  <div class="group-right">
    <div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/689/title/und/teaser"><div class="field-item"> LUKE SHARP: NZ #56</div></div></h2></div></div></div><div data-edit-id="node/689/field_sponsors/und/teaser" class="field field-name-field-sponsors field-type-text-long field-label-inline clearfix"><div class="field-label">Sponsors:&nbsp;</div><div class="field-items"><div class="field-item even">PROMT Parts, Outboard Marine Technologies Ltd, ZENITHTECNICA, Kozmik Marine Finishes</div></div></div><div data-edit-id="node/689/field_hull_manufacturer/und/teaser" class="field field-name-field-hull-manufacturer field-type-text field-label-inline clearfix"><div class="field-label">Hull Manufacturer:&nbsp;</div><div class="field-items"><div class="field-item even">Composite F1</div></div></div><div data-edit-id="node/689/field_home_town/und/teaser" class="field field-name-field-home-town field-type-text field-label-inline clearfix"><div class="field-label">Home Town:&nbsp;</div><div class="field-items"><div class="field-item even">New Zealand</div></div></div><div data-edit-id="node/689/field_team_website/und/teaser" class="field field-name-field-team-website field-type-text field-label-inline clearfix"><div class="field-label">Team Website:&nbsp;</div><div class="field-items"><div class="field-item even">www.lukesharpracing.co.nz</div></div></div>  </div>

</div>

  </div>



  <div class="views-row views-row-3 views-row-odd">
    <div class="ds-2col node node-drivers node-teaser odd  view-mode-teaser clearfix" typeof="sioc:Item foaf:Document" about="/drivers/paul-eade-budlight-41">

  
  <div class="group-left">
    <div data-edit-id="node/29/field_photos/und/teaser" class="field field-name-field-photos field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img width="249" height="150" alt="" src="http://www.formulapowerboats.com.au/sites/default/files/styles/profile_boat/public/Aussie%20Champ%2C%20Paul%20eade%20holds%20the%20f1%20series%20lead%20after%20round%201..JPG?itok=vBYLtye8" typeof="foaf:Image"></div></div></div><div class="view view--eva-extra-image-on-driver-profile view-id-_eva_extra_image_on_driver_profile view-display-id-entity_view_1 view-dom-id-23eb099313cd6891f7988648b595d944">
      
  
  
      <div class="view-content">
          <ul>          <li class="">  
          <img width="120" height="150" alt="" src="http://www.formulapowerboats.com.au/sites/default/files/styles/profile_face/public/F1%20Paul%20Eade.jpg?itok=_7C4WS3F" typeof="foaf:Image">  </li>
      </ul>    </div>
  
  
  
  
  
  
</div>   </div>

  <div class="group-right">
    <div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/29/title/und/teaser"><div class="field-item"> PAUL EADE: BudLight #41</div></div></h2></div></div></div><div data-edit-id="node/29/field_hull_manufacturer/und/teaser" class="field field-name-field-hull-manufacturer field-type-text field-label-inline clearfix"><div class="field-label">Hull Manufacturer:&nbsp;</div><div class="field-items"><div class="field-item even">GTR</div></div></div>  </div>

</div>

  </div>




  <div class="views-row views-row-4 views-row-even">
    <div class="ds-2col node node-drivers node-teaser even  view-mode-teaser clearfix" typeof="sioc:Item foaf:Document" about="/drivers/doug-smith-djs-contracting-23">

  
  <div class="group-left">
    <div class="view view--eva-extra-image-on-driver-profile view-id-_eva_extra_image_on_driver_profile view-display-id-entity_view_1 view-dom-id-678999dad55136c758fcfab9e6404b02">
      
  
  
      <div class="view-content">
          <ul>          <li class="">  
            </li>
      </ul>    </div>
  
  
  
  
  
  
</div>   </div>

  <div class="group-right">
    <div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/696/title/und/teaser"><div class="field-item"> DOUG SMITH: DJS CONTRACTING #23</div></div></h2></div></div></div><div data-edit-id="node/696/field_sponsors/und/teaser" class="field field-name-field-sponsors field-type-text-long field-label-inline clearfix"><div class="field-label">Sponsors:&nbsp;</div><div class="field-items"><div class="field-item even">DJS Contracting</div></div></div><div data-edit-id="node/696/field_home_town/und/teaser" class="field field-name-field-home-town field-type-text field-label-inline clearfix"><div class="field-label">Home Town:&nbsp;</div><div class="field-items"><div class="field-item even">Sydney, NSW</div></div></div>  </div>

</div>

  </div>





  <div class="views-row views-row-5 views-row-odd views-row-last">
    <div class="ds-2col node node-drivers node-teaser odd  view-mode-teaser clearfix" typeof="sioc:Item foaf:Document" about="/drivers/david-ainley-express-design-racing-33">

  
  <div class="group-left">
    <div data-edit-id="node/33/field_photos/und/teaser" class="field field-name-field-photos field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img width="249" height="150" alt="" src="http://www.formulapowerboats.com.au/sites/default/files/styles/profile_boat/public/swfimage_13487_1.jpg?itok=h8hXnv2-" typeof="foaf:Image"></div></div></div><div class="view view--eva-extra-image-on-driver-profile view-id-_eva_extra_image_on_driver_profile view-display-id-entity_view_1 view-dom-id-83a7c4ccc103734410d06840950726a9">
      
  
  
      <div class="view-content">
          <ul>          <li class="">  
          <img width="120" height="150" alt="" src="http://www.formulapowerboats.com.au/sites/default/files/styles/profile_face/public/swfimage_13487_headshot_1.jpg?itok=LScJlJii" typeof="foaf:Image">  </li>
      </ul>    </div>
  
  
  
  
  
  
</div>   </div>

  <div class="group-right">
    <div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/33/title/und/teaser"><div class="field-item"> DAVID AINLEY: Express Design Racing #33</div></div></h2></div></div></div><div data-edit-id="node/33/field_sponsors/und/teaser" class="field field-name-field-sponsors field-type-text-long field-label-inline clearfix"><div class="field-label">Sponsors:&nbsp;</div><div class="field-items"><div class="field-item even">Express Design Vinyl Graffix, Queensland Outboards, Stripe Supplies Kierajay Border Collies, Spray Tech Marine, </div></div></div><div data-edit-id="node/33/field_hull_manufacturer/und/teaser" class="field field-name-field-hull-manufacturer field-type-text field-label-inline clearfix"><div class="field-label">Hull Manufacturer:&nbsp;</div><div class="field-items"><div class="field-item even">GTR</div></div></div><div data-edit-id="node/33/field_home_town/und/teaser" class="field field-name-field-home-town field-type-text field-label-inline clearfix"><div class="field-label">Home Town:&nbsp;</div><div class="field-items"><div class="field-item even">Brisbane, QLD</div></div></div><div data-edit-id="node/33/field_occupation/und/teaser" class="field field-name-field-occupation field-type-text field-label-inline clearfix"><div class="field-label">Occupation:&nbsp;</div><div class="field-items"><div class="field-item even">Sign Writer</div></div></div><div data-edit-id="node/33/field_team_website/und/teaser" class="field field-name-field-team-website field-type-text field-label-inline clearfix"><div class="field-label">Team Website:&nbsp;</div><div class="field-items"><div class="field-item even">www.expressdesign.com.au</div></div></div>  </div>

</div>

  </div>

-->






    </div>
  
  
  
  
  
  
</div>    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /content-inner -->
  </div><!-- /content -->
                                                      </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->
                                      </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div>
            
<?php get_footer(); ?>