<?php
/*
  Template Name: Contact Us
*/
?>
<?php get_header(); ?>

<div class="main-wrapper full-width clearfix" id="main-wrapper">
        <div class="main region grid16-16" id="main">
          <div class="main-inner inner clearfix" id="main-inner">
            
            <!-- main group: width = grid_width - sidebar_first_width -->
            <div class="main-group region nested grid16-16" id="main-group">
              <div class="main-group-inner inner" id="main-group-inner">
                
                <div class="main-content region nested" id="main-content">
                  <div class="main-content-inner inner" id="main-content-inner">
                    <!-- content group: width = grid_width - sidebar_first_width - sidebar_second_width -->
                    <div class="content-group region nested grid16-16" id="content-group">
                      <div class="content-group-inner inner" id="content-group-inner">
                                                
                        <div class="content-region region nested" id="content-region">
                          <div class="content-region-inner inner" id="content-region-inner">
                            <a id="main-content-area"></a>
                                                                                                                                            <h1 class="title gutter"><div data-edit-id="node/11/title/und/full"><div class="field-item">Contact Us</div></div></h1>
                                                                                                                                              
<!-- content region -->
  <div class="region region-content content nested grid16-16" id="content">
    <div class="content-inner inner" id="content-inner">
      <div class="block block-system first last odd" id="block-system-main">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="ds-2col node node-page odd full-node view-mode-full clearfix" typeof="foaf:Document" about="/contact-us">

  
  <div class="group-left">
    <div data-edit-id="node/11/body/und/full" class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even">



        <?php /* Start the Loop */ ?>
        <?php while ( have_posts() ) : the_post(); ?>       

            <?php the_content(); ?>

        <?php endwhile; ?>


<!--

      <p><strong>&#8203;</strong><strong>Postal Address:</strong><br>
  PO&nbsp;BOX&nbsp;352<br>
  Raymond Terrace NSW 2324</p>

<table cellspacing="\&quot;\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;1\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\&quot;\&quot;" cellpadding="\&quot;\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;1\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\&quot;\&quot;" border="\&quot;\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;0\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\&quot;\&quot;" td="\&quot;\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\&quot;\&quot;" style="\&quot;\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;vertical-align:\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\&quot;\&quot;"></table><p><strong>President:&nbsp;</strong><br>
  Gavin Simmons<br><font color="#3e454c" face="Helvetica, Arial, lucida grande, tahoma, verdana, arial, sans-serif"><span style="font-size: 12px; line-height: 15.3599996566772px; white-space: pre-wrap; background-color: rgb(247, 247, 247);">0437 613 909</span></font><br>
  president@formulapowerboats.com.au<br><br><strong>Vice President:&nbsp;</strong><br>
  Glenn Burns<br>
  vicepresident@formulapowerboats.com.au&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>

<p><strong>Secretary:&nbsp;</strong><br>
  Danielle Martin<br>
  secretary@formulapowerboats.com.au&nbsp;&nbsp;</p>

<p><strong>Treasurer:&nbsp;</strong><br>
  Robyn Cleary<br>
  treasurer@formulapowerboats.com.au&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

<p><strong>Media Officer:&nbsp;</strong><br>
  Mike Cleary<br>
  media@formulapowerboats.com.au&nbsp;&nbsp;<br><br><strong>Entries &amp; Memberships:</strong><br>
  Rebecca Fowler<br>
  entriesandmemberships@formulapowerboats.com.au&nbsp;</p>

<p><strong>Website Admin:</strong><br>
  Brock Cohen&nbsp;<br>
  websiteadmin@formulapowerboats.com.au&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
-->
<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table cellspacing="\&quot;\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;1\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\&quot;\&quot;" cellpadding="\&quot;\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;1\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\&quot;\&quot;" border="\&quot;\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;0\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\&quot;\&quot;" td="\&quot;\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\&quot;\&quot;" style="\&quot;\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&quot;vertical-align:\\\\\\\\\\\\\\\\\\\\\\\\&quot;\\\\\\&quot;\&quot;"></table></div></div></div>  </div>

  <div class="group-right">
    <div data-edit-id="node/11/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items">

<?php

// check if the repeater field has rows of data
if( have_rows('contact_side_images') ):

  // loop through the rows of data
    while ( have_rows('contact_side_images') ) : the_row();

        // display a sub field value
        //the_sub_field('sub_field_name'); ?>
      <div class="field-item even about_side_img"><img alt="image" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></div>

<?php

    endwhile;

else :

    // no rows found

endif;

?>



      <div class="field-item even"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Contact%20us%201_0.jpg?itok=dl1EOaNO" typeof="foaf:Image"></div><div class="field-item odd"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Contact%20us%202_0.jpg?itok=2OO3mVed" typeof="foaf:Image"></div><div class="field-item even"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Contact%20us%203.jpg?itok=J0Iq8UJw" typeof="foaf:Image"></div>

    </div></div>  </div>

</div>

    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /content-inner -->
  </div><!-- /content -->
                                                      </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->
                                      </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div>
            
<?php get_footer(); ?>