<?php
/*
  Template Name: Drivers
*/
?>
<?php get_header(); ?>

<div class="main-wrapper full-width clearfix" id="main-wrapper">
        <div class="main region grid16-16" id="main">
          <div class="main-inner inner clearfix" id="main-inner">
            
            <!-- main group: width = grid_width - sidebar_first_width -->
            <div class="main-group region nested grid16-16" id="main-group">
              <div class="main-group-inner inner" id="main-group-inner">
                
                <div class="main-content region nested" id="main-content">
                  <div class="main-content-inner inner" id="main-content-inner">
                    <!-- content group: width = grid_width - sidebar_first_width - sidebar_second_width -->
                    <div class="content-group region nested grid16-16" id="content-group">
                      <div class="content-group-inner inner" id="content-group-inner">
                                                
                        <div class="content-region region nested" id="content-region">
                          <div class="content-region-inner inner" id="content-region-inner">
                            <a id="main-content-area"></a>
                                                                                                                                                                                                                                  
<!-- content region -->
  <div class="region region-content content nested grid16-16" id="content">
    <div class="content-inner inner" id="content-inner">
      <div class="block block-system first last odd" id="block-system-main">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="view view-divisions view-id-divisions view-display-id-page view-dom-id-bcc503ffa6a7ba2b0e329014f48fb654">
        
  
  
      <div class="view-content">

<?php

$odd_even = 'views-row-odd'; 
$counter = 1;

// The Query
$args = array(
    //'post_type' => 'post',
    'posts_per_page' => 10,
    'post_type' => 'driver',
    //'cat' => 3
  );
$the_query = new WP_Query( $args );

// The Loop
if ( $the_query->have_posts() ) {

  while ( $the_query->have_posts() ) {
    $the_query->the_post();
    if($counter % 2 == 1) {
      $odd_even = 'views-row-odd';
    } else {
      $odd_even = 'views-row-even';
    }
    //echo '<li>' . get_the_title() . '</li>'; ?>      

        <div class="views-row views-row-1 <?php echo $odd_even; ?> views-row-first">
    <div class="ds-1col taxonomy-term vocabulary-powerboat-divisions view-mode-full clearfix">

  
  <div class="field field-name-my-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><h2><?php the_title(); ?></h2></div></div></div><div data-edit-id="taxonomy_term/1/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><?php the_post_thumbnail('formula-latest-news'); ?></div></div></div><div class="taxonomy-term-description"><p><?php echo formula_get_excerpt(280); ?></p>
</div><div class="view view-powerboat-division-eva-link-to-drivers- view-id-powerboat_division_eva_link_to_drivers_ view-display-id-entity_view_1 view-dom-id-0c58fa95b8b1f6d8f22a4a7522f2ec3c">
      
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-name">        <span class="field-content"><a href="<?php the_permalink(); ?>">View Drivers</a></span>  </div>  </div>
    </div>
</div> </div>

  </div>

<?php
  }
    /* Restore original Post Data */
  wp_reset_postdata();
} else {
  // no posts found
}

?>


<!--
        <div class="views-row views-row-1 views-row-odd views-row-first">
    <div class="ds-1col taxonomy-term vocabulary-powerboat-divisions view-mode-full clearfix">

  
  <div class="field field-name-my-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><h2>Formula One</h2></div></div></div><div data-edit-id="taxonomy_term/1/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img width="275" height="160" alt="" src="<?php echo get_template_directory_uri(); ?>/images/10537122_10203407515360930_340508402845889605_n.jpg?itok=QF9zBbln" typeof="foaf:Image"></div></div></div><div class="taxonomy-term-description"><p>Formula One has always been known as ‘the elite” in Powerboat racing. With speeds exceeding 230km/h,&nbsp;they can accelerate to 160km/h&nbsp;in less than 4 seconds and hit 5G’s on a corner. Constructed mainly of&nbsp;Carbon Fibre they are the ultimate in acceleration&nbsp;and exhilaration.</p>
</div><div class="view view-powerboat-division-eva-link-to-drivers- view-id-powerboat_division_eva_link_to_drivers_ view-display-id-entity_view_1 view-dom-id-0c58fa95b8b1f6d8f22a4a7522f2ec3c">
      
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-name">        <span class="field-content"><a href="/drivers/formula-one">View Drivers</a></span>  </div>  </div>
    </div>
</div> </div>

  </div>





  <div class="views-row views-row-2 views-row-even">
    <div class="ds-1col taxonomy-term vocabulary-powerboat-divisions view-mode-full clearfix">

  
  <div class="field field-name-my-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><h2>Formula Two (SST120)</h2></div></div></div><div data-edit-id="taxonomy_term/2/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img width="275" height="160" alt="" src="<?php echo get_template_directory_uri(); ?>/images/10400055_967674176618115_768291162516638250_n.jpg?itok=gRsCpiQg" typeof="foaf:Image"></div></div></div><div class="taxonomy-term-description"><p>Formula Two&nbsp;Powerboats are the official feeder category to Formula One. These boats are 4.8&nbsp;metre tunnel&nbsp;hulls, similar to F1 boats. They weigh 500kgs and are propelled by a 200 horsepower race production engine, the&nbsp;Mercury SST120.</p>
</div><div class="view view-powerboat-division-eva-link-to-drivers- view-id-powerboat_division_eva_link_to_drivers_ view-display-id-entity_view_1 view-dom-id-52b34e6c032b20573dcc8b0bc85cfd5b">
      
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-name">        <span class="field-content"><a href="/drivers/formula-two-%28sst120%29">View Drivers</a></span>  </div>  </div>
    </div>
</div> </div>

  </div>




  <div class="views-row views-row-3 views-row-odd">
    <div class="ds-1col taxonomy-term vocabulary-powerboat-divisions view-mode-full clearfix">

  
  <div class="field field-name-my-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><h2>Formula Optimax</h2></div></div></div><div data-edit-id="taxonomy_term/14/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img width="275" height="160" alt="" src="<?php echo get_template_directory_uri(); ?>/images/10994589_904181042967429_7162226093433030352_n.jpg?itok=RbUEBgXc" typeof="foaf:Image"></div></div></div><div class="taxonomy-term-description"><p>Formula Optimax&nbsp;boats are all powered by Optimax 200XS SST outboards that were developed by Mercury Marine as a green, environmentally friendly race engine. Using the very latest in 2&nbsp;stoke technology these engines have a capacity of 2.5L producing 200hp with a maximum of 8000rpm and a top speed exceeding 200kph.&nbsp;Formula OptiMax is the current UIM Formula Two class.<br><br>
  &nbsp;</p>
</div><div class="view view-powerboat-division-eva-link-to-drivers- view-id-powerboat_division_eva_link_to_drivers_ view-display-id-entity_view_1 view-dom-id-165925f41b53554f1c07ae39c994da6c">
      
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-name">        <span class="field-content"><a href="/drivers/formula-optimax">View Drivers</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div> </div>

  </div>
  <div class="views-row views-row-4 views-row-even">
    <div class="ds-1col taxonomy-term vocabulary-powerboat-divisions view-mode-full clearfix">

  
  <div class="field field-name-my-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><h2>Formula Three</h2></div></div></div><div data-edit-id="taxonomy_term/3/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img width="275" height="160" alt="" src="<?php echo get_template_directory_uri(); ?>/images/_BKA4118.jpg?itok=ZascUZkB" typeof="foaf:Image"></div></div></div><div class="taxonomy-term-description"><p>Formula 3&nbsp;Powerboats are 3.5&nbsp;metre tunnel hulls mostly constructed of timber other than the cockpit which is the same materials as the Formula One’s.&nbsp;They are powered by engines restricted to 1250cc in capacity, reaching top speeds of 160km/h.</p>
<p>&nbsp;</p>
</div><div class="view view-powerboat-division-eva-link-to-drivers- view-id-powerboat_division_eva_link_to_drivers_ view-display-id-entity_view_1 view-dom-id-327e535fa6f170b3944713ae41642baa">
      
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-name">        <span class="field-content"><a href="/drivers/formula-three">View Drivers</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div> </div>

  </div>
  <div class="views-row views-row-5 views-row-odd">
    <div class="ds-1col taxonomy-term vocabulary-powerboat-divisions view-mode-full clearfix">

  
  <div class="field field-name-my-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><h2>Formula Four</h2></div></div></div><div data-edit-id="taxonomy_term/23/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img width="275" height="160" alt="" src="<?php echo get_template_directory_uri(); ?>/images/BKA_9004.jpg?itok=SsdbXI7b" typeof="foaf:Image"></div></div></div><div class="taxonomy-term-description"><p>F4S is the newest class to Australia. These boats are powered by a 60hp 4-Stroke Mercury Formula Race engine reaching top speeds of 110km/h&nbsp;and have a minimum weight (including driver) of 350kg.&nbsp;Drivers can start from&nbsp;the age of 14 as long as they&nbsp;have had at least two years experience in the J3 Formula Future Class.<br><br>
  &nbsp;</p>
</div><div class="view view-powerboat-division-eva-link-to-drivers- view-id-powerboat_division_eva_link_to_drivers_ view-display-id-entity_view_1 view-dom-id-a80730330683e422d3f214cc49ac96e1">
      
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-name">        <span class="field-content"><a href="/drivers/formula-four">View Drivers</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div> </div>

  </div>
  <div class="views-row views-row-6 views-row-even views-row-last">
    <div class="ds-1col taxonomy-term vocabulary-powerboat-divisions view-mode-full clearfix">

  
  <div class="field field-name-my-title field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><h2>Formula Futures</h2></div></div></div><div data-edit-id="taxonomy_term/6/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div class="field-item even"><img width="275" height="160" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Junior%20home%20page.jpg?itok=Kal4IL9E" typeof="foaf:Image"></div></div></div><div class="taxonomy-term-description"><p>These drivers are our champions of the future. Three classes compete within each race:<br>
J1: 6hp (8 - 10years)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; J2: 6hp &ndash; 9.9hp (10 - 12years)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; J3: 15hp (12 &ndash; 16years)<br>
The 3 levels provide the stepping-stones for our children to enjoy racing in a safe and structured environment.</p>
</div><div class="view view-powerboat-division-eva-link-to-drivers- view-id-powerboat_division_eva_link_to_drivers_ view-display-id-entity_view_1 view-dom-id-638e72b502d8eb402255393eeb101ada">
      
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
      
  <div class="views-field views-field-name">        <span class="field-content"><a href="/drivers/formula-futures">View Drivers</a></span>  </div>  </div>
    </div>
  
  
  
  
  
  
</div> </div>

  </div>
-->



    </div>
  
  
  
  
  
  
</div>    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /content-inner -->
  </div><!-- /content -->
                                                      </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->
                                      </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div>
            
<?php get_footer(); ?>