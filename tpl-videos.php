<?php
/*
  Template Name: Videos
*/
?>
<?php get_header(); ?>

<div class="main-wrapper full-width clearfix" id="main-wrapper">
        <div class="main region grid16-16" id="main">
          <div class="main-inner inner clearfix" id="main-inner">
            
            <!-- main group: width = grid_width - sidebar_first_width -->
            <div class="main-group region nested grid16-16" id="main-group">
              <div class="main-group-inner inner" id="main-group-inner">
                
                <div class="main-content region nested" id="main-content">
                  <div class="main-content-inner inner" id="main-content-inner">
                    <!-- content group: width = grid_width - sidebar_first_width - sidebar_second_width -->
                    <div class="content-group region nested grid16-10" id="content-group">
                      <div class="content-group-inner inner" id="content-group-inner">
                                                
                        <div class="content-region region nested" id="content-region">
                          <div class="content-region-inner inner" id="content-region-inner">
                            <a id="main-content-area"></a>
                                                                                                                                                                                                                                  
<!-- content region -->
  <div class="region region-content content nested grid16-10" id="content">
    <div class="content-inner inner" id="content-inner">
      <div class="block block-system first last odd" id="block-system-main">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="view view-videos view-id-videos view-display-id-page view-dom-id-2ec11c7328a6ffa8daa95eddfd5a681a">
        
  
  
      <div class="view-content">
        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
    <div class="ds-1col node node-video-collection odd full-node view-mode-full clearfix" typeof="sioc:Item foaf:Document" about="/video/afpgp-we-can-make-world-stop">

  
  <div data-edit-id="node/404/field_featured_video/und/full" class="field field-name-field-featured-video field-type-youtube field-label-hidden"><div class="field-items"><div class="field-item even"><iframe width="560" height="335" frameborder="0" allowfullscreen="" src="http://www.youtube.com/embed/_NPS_7g99ns?wmode=opaque&amp;rel=0"></iframe></div></div></div><div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/404/title/und/full"><div class="field-item">AFPGP | We Can Make The World Stop</div></div></h2></div></div></div></div>

  </div>
    </div>
  
  
  
  
  
  
</div>    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /content-inner -->
  </div><!-- /content -->
                                                      </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->
                    
<!-- sidebar-second region -->
  <div class="region region-sidebar-second sidebar-second nested grid16-6 sidebar" id="sidebar-second">
    <div class="sidebar-second-inner inner" id="sidebar-second-inner">
      <div class="block block-quicktabs first last odd" id="block-quicktabs-video_manager">
  <div class="gutter inner clearfix">
            <h2 class="title block-title">Video Manager</h2>
        
    <div class="content clearfix">
      <div class="quicktabs-wrapper quicktabs-style-sky jquery-once-1-processed" id="quicktabs-video_manager"><div class="item-list"><ul class="quicktabs-tabs quicktabs-style-sky"><li class="active first"><a class="active quicktabs-loaded jquery-once-2-processed" id="quicktabs-tab-video_manager-0" href="/videos?qt-video_manager=0#qt-video_manager">Official Highlights</a></li>
<li><a class="active jquery-once-2-processed" id="quicktabs-tab-video_manager-1" href="/videos?qt-video_manager=1#qt-video_manager">Other Footage</a></li>
<li class="last"><a class="active jquery-once-2-processed" id="quicktabs-tab-video_manager-2" href="/videos?qt-video_manager=2#qt-video_manager">Micellaneous</a></li>
</ul></div><div class="quicktabs_main quicktabs-style-sky" id="quicktabs-container-video_manager"><div class="quicktabs-tabpage " id="quicktabs-tabpage-video_manager-0"><div class="block block-views" id="block-views-video_list-block">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="view view-video-list view-id-video_list view-display-id-block view-dom-id-87b3724ea9f0f94c0249bcc71feafa91">
        
  
  
      <div class="view-content">
      <table class="views-table cols-0">
        <caption>2013-2014</caption>
      <tbody>
          <tr class="odd views-row-first views-row-last">
                  <td class="views-field views-field-title">
            <a href="/video/afpgp-we-can-make-world-stop">AFPGP | We Can Make The World Stop</a>          </td>
              </tr>
      </tbody>
</table>
<table class="views-table cols-0">
        <caption>2012-2013</caption>
      <tbody>
          <tr class="odd">
                  <td class="views-field views-field-title">
            <a href="/video/round-2-wagga">Round 2 | Wagga</a>          </td>
              </tr>
          <tr class="even views-row-last">
                  <td class="views-field views-field-title">
            <a href="/video/round-4-taree">Round 4 | Taree</a>          </td>
              </tr>
      </tbody>
</table>
<table class="views-table cols-0">
        <caption>2011-2012</caption>
      <tbody>
          <tr class="odd">
                  <td class="views-field views-field-title">
            <a href="/video/round-1-taree">Round 1 | Taree</a>          </td>
              </tr>
          <tr class="even views-row-last">
                  <td class="views-field views-field-title">
            <a href="/video/round-2-grafton">Round 2 | Grafton</a>          </td>
              </tr>
          <tr class="odd">
                  <td class="views-field views-field-title">
            <a href="/video/round-3-wagga">Round 3 | Wagga</a>          </td>
              </tr>
          <tr class="even">
                  <td class="views-field views-field-title">
            <a href="/video/round-4-port-macquarie">Round 4 | Port Macquarie</a>          </td>
              </tr>
      </tbody>
</table>
<table class="views-table cols-0">
        <caption>2010-2011</caption>
      <tbody>
          <tr class="odd">
                  <td class="views-field views-field-title">
            <a href="/video/round-2-grafton-0">Round 2 | Grafton</a>          </td>
              </tr>
          <tr class="even">
                  <td class="views-field views-field-title">
            <a href="/video/round-3-port-macquarie">Round 3 | Port Macquarie</a>          </td>
              </tr>
      </tbody>
</table>
    </div>
  
  
  
  
  
  
</div>    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
</div><div class="quicktabs-tabpage quicktabs-hide" id="quicktabs-tabpage-video_manager-1"><div class="block block-views" id="block-views-video_list-block_1">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="view view-video-list view-id-video_list view-display-id-block_1 view-dom-id-5a8b6e76022be53030029899045585dd">
        
  
  
      <div class="view-content">
      <table class="views-table cols-0">
        <caption>2012-2013</caption>
      <tbody>
          <tr class="odd views-row-first views-row-last">
                  <td class="views-field views-field-title">
            <a href="/video/prostock-sonter-vs-davoll-taree-june-2013">Prostock Sonter vs Davoll - Taree June 2013</a>          </td>
              </tr>
      </tbody>
</table>
<table class="views-table cols-0">
        <caption>2011-2012</caption>
      <tbody>
          <tr class="odd">
                  <td class="views-field views-field-title">
            <a href="/video/formula-2-sst120-australia-highlights-package-2011">Formula 2 SST120 Australia Highlights Package 2011</a>          </td>
              </tr>
          <tr class="even views-row-last">
                  <td class="views-field views-field-title">
            <a href="/video/budlight-crash-taree-feb-2012">Budlight Crash - Taree Feb 2012</a>          </td>
              </tr>
      </tbody>
</table>
    </div>
  
  
  
  
  
  
</div>    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
</div><div class="quicktabs-tabpage quicktabs-hide" id="quicktabs-tabpage-video_manager-2"><div class="block block-views" id="block-views-video_list-block_2">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="view view-video-list view-id-video_list view-display-id-block_2 view-dom-id-e54ea9c5112feac53fb659da069ed565">
        
  
  
      <div class="view-content">
      <table class="views-table cols-0">
        <caption>2012-2013</caption>
      <tbody>
          <tr class="odd views-row-first">
                  <td class="views-field views-field-title">
            <a href="/video/cohen-bros-racing-f3-unveiling-2013">Cohen Bros Racing - F3 Unveiling 2013</a>          </td>
              </tr>
          <tr class="even">
                  <td class="views-field views-field-title">
            <a href="/video/sst120-r3-uhpbc-feb-2013">SST120 - R3 UHPBC Feb 2013</a>          </td>
              </tr>
          <tr class="odd">
                  <td class="views-field views-field-title">
            <a href="/video/brock-cohen-550cc-race-start-june-2013">Brock Cohen 550cc Race Start June 2013</a>          </td>
              </tr>
          <tr class="even">
                  <td class="views-field views-field-title">
            <a href="/video/andrew-king-sst120-crash-dargle-june-2013">Andrew King SST120 Crash - Dargle June 2013</a>          </td>
              </tr>
          <tr class="odd">
                  <td class="views-field views-field-title">
            <a href="/video/rhys-coles-dargle-gold-cup-june-2013">Rhys Coles - Dargle Gold Cup June 2013</a>          </td>
              </tr>
          <tr class="even views-row-last">
                  <td class="views-field views-field-title">
            <a href="/video/craig-bailey-vs-wayne-smith-crash-champions-champions-st-george-july-2013">Craig Bailey vs Wayne Smith Crash "Champions of Champions" St George July 2013</a>          </td>
              </tr>
      </tbody>
</table>
<table class="views-table cols-0">
        <caption>2011-2012</caption>
      <tbody>
          <tr class="odd">
                  <td class="views-field views-field-title">
            <a href="/video/craig-bailey-king-river-dargle-feb-2012">Craig Bailey in King of the River Dargle Feb 2012</a>          </td>
              </tr>
          <tr class="even">
                  <td class="views-field views-field-title">
            <a href="/video/connolly-racing-team-automotive-cessnock-package">Connolly Racing - Team Automotive Cessnock Package</a>          </td>
              </tr>
          <tr class="odd">
                  <td class="views-field views-field-title">
            <a href="/video/budlight-rollover-arch-spooner-june-2012">Budlight Rollover in Arch Spooner June 2012</a>          </td>
              </tr>
      </tbody>
</table>
<table class="views-table cols-0">
        <caption>2010-2011</caption>
      <tbody>
          <tr class="odd">
                  <td class="views-field views-field-title">
            <a href="/video/connolly-vs-bonnici-uhpbc-july-2011">Connolly vs Bonnici UHPBC July 2011</a>          </td>
              </tr>
      </tbody>
</table>
    </div>
  
  
  
  
  
  
</div>    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
</div></div></div>    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /sidebar-second-inner -->
  </div><!-- /sidebar-second -->
                  </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                
<!-- postscript-top region -->
  <div class="region region-postscript-top postscript-top nested grid16-16" id="postscript-top">
    <div class="postscript-top-inner inner" id="postscript-top-inner">
      <div class="block block-block first  odd" id="block-block-10">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <p><img style="width: 300px; height: 200px; border-width: 0px; border-style: solid; margin: 0px; float: left;" src="<?php echo get_template_directory_uri(); ?>/images/Commentators%20-%20Matt.jpg" alt=""></p>
    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
<div class="block block-block   even" id="block-block-11">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <p class="rtecenter"><img style="width: 300px; height: 200px; border-width: 0px; border-style: solid; margin: 0px;" src="<?php echo get_template_directory_uri(); ?>/images/Commentators%20Craig%20%26%20Dave.jpg" alt=""></p>
    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
<div class="block block-block  last odd" id="block-block-12">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <p><img style="width: 300px; height: 199px; border-width: 0px; border-style: solid; margin: 0px;" src="<?php echo get_template_directory_uri(); ?>/images/Commentators%20Ricky%20%26%20Craig.jpg" alt=""></p>
    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /postscript-top-inner -->
  </div><!-- /postscript-top -->
              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div>
            
<?php get_footer(); ?>