function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

Date.firstDayOfWeek = 0;
Date.format = 'yyyy-mm-dd';

jQuery(document).ready(function($) {

	/*if($('body').hasClass('home')) {
		$('.cal_cont_2017 .view-content').css('min-height', ($('#content-group-inner').height() - 60) + 'px');
	}*/

	$('.date-pick').datePicker({clickInput:true})

	$('.sig_area').signature();

	$('.bxslider').bxSlider();

	console.log('test scripts2');

	$('.owl-carousel').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:false,
	    dots: false,
	    autoplay: true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});

	$('.owl-carousel2').owlCarousel({
	    loop:true,
	    margin:0,
	    nav:false,
	    dots: false,
	    autoplay: true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:3
	        }
	    }
	});

	/*$('#formula_submit_id').hover(
		function() {
			$('#custom_id').val( $('#m_username_id').val() + '|||' + $('#m_email_id').val() );
		},
		function() {

		}
	);*/



});

jQuery(window).load(function() {
	jQuery('.home_slider').css('visibility','visible');
});