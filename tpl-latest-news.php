<?php
/*
  Template Name: Latest News
*/
?>
<?php get_header(); ?>

<div class="main-wrapper full-width clearfix" id="main-wrapper">
        <div class="main region grid16-16" id="main">
          <div class="main-inner inner clearfix" id="main-inner">
            
            <!-- main group: width = grid_width - sidebar_first_width -->
            <div class="main-group region nested grid16-16" id="main-group">
              <div class="main-group-inner inner" id="main-group-inner">
                
                <div class="main-content region nested" id="main-content">
                  <div class="main-content-inner inner" id="main-content-inner">
                    <!-- content group: width = grid_width - sidebar_first_width - sidebar_second_width -->
                    <div class="content-group region nested grid16-16" id="content-group">
                      <div class="content-group-inner inner" id="content-group-inner">
                                                
                        <div class="content-region region nested" id="content-region">
                          <div class="content-region-inner inner" id="content-region-inner">
                            <a id="main-content-area"></a>
                                                                                                                                            <h1 class="title gutter">Latest News</h1>
                                                                                                                                              
<!-- content region -->
  <div class="region region-content content nested grid16-16" id="content">
    <div class="content-inner inner" id="content-inner">
      <div class="block block-system first last odd" id="block-system-main">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="view view-latest-news view-id-latest_news view-display-id-page view-dom-id-e9bd933834415a2998777247bd19920c">
        
  
  
      <div class="view-content">

<?php

// The Query
$args = array(
    'post_type' => 'post',
    'posts_per_page' => 5
  );
$the_query = new WP_Query( $args );

// The Loop
if ( $the_query->have_posts() ) {

  while ( $the_query->have_posts() ) {
    $the_query->the_post();
    //echo '<li>' . get_the_title() . '</li>'; ?>
            <div class="views-row views-row-1 views-row-odd views-row-first">
        <div class="ds-1col node node-article node-promoted node-teaser odd  view-mode-teaser clearfix" typeof="sioc:Item foaf:Document" about="/articles/drama-filled-2016-grand-final-round-%E2%80%93-port-macquarie">

      
      <div data-edit-id="node/748/field_image/und/teaser" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div resource="<?php echo get_template_directory_uri(); ?>/images/20160807_100253_001.jpg?itok=4fxMV8_B" rel="og:image rdfs:seeAlso" class="field-item even"><?php the_post_thumbnail('formula-latest-news'); ?></a></div></div></div><div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/748/title/und/teaser"><div class="field-item"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div></div></h2></div></div></div><div data-edit-id="node/748/body/und/teaser" class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even"><p><?php echo formula_get_excerpt(320); ?></p></div></div></div><div class="field field-name-node-link field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><a href="<?php the_permalink(); ?>">Read more</a></div></div></div></div>

      </div>
<?php
  }
?>



<!--
        <div class="views-row views-row-1 views-row-odd views-row-first">
    <div class="ds-1col node node-article node-promoted node-teaser odd  view-mode-teaser clearfix" typeof="sioc:Item foaf:Document" about="/articles/drama-filled-2016-grand-final-round-%E2%80%93-port-macquarie">

  
  <div data-edit-id="node/748/field_image/und/teaser" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div resource="<?php echo get_template_directory_uri(); ?>/images/20160807_100253_001.jpg?itok=4fxMV8_B" rel="og:image rdfs:seeAlso" class="field-item even"><a href="/articles/drama-filled-2016-grand-final-round-%E2%80%93-port-macquarie"><img width="275" height="155" alt="" src="<?php echo get_template_directory_uri(); ?>/images/20160807_100253_001.jpg?itok=4fxMV8_B" typeof="foaf:Image"></a></div></div></div><div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/748/title/und/teaser"><div class="field-item"><a href="/articles/drama-filled-2016-grand-final-round-%E2%80%93-port-macquarie">DRAMA FILLED 2016 GRAND FINAL ROUND &ndash; PORT MACQUARIE</a></div></div></h2></div></div></div><div data-edit-id="node/748/body/und/teaser" class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even"><p>The Grand Final Round of the 2016 Australian Formula Powerboat Grand Prix Series certainly provided plenty of drama and spectacular action on the Hastings River at Port Macquarie on August 6 &amp; 7. With all, but one Formula Series Championship to be decided along with a record number of final round entries, the anticipation, tension and excitement levels were certainly at a high level.</p></div></div></div><div class="field field-name-node-link field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/articles/drama-filled-2016-grand-final-round-%E2%80%93-port-macquarie">Read more</a></div></div></div></div>

  </div>
  <div class="views-row views-row-2 views-row-even">
    <div class="ds-1col node node-article node-promoted node-teaser even  view-mode-teaser clearfix" typeof="sioc:Item foaf:Document" about="/articles/port-macquarie-%E2%80%93-grand-final-round">

  
  <div data-edit-id="node/738/field_image/und/teaser" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div resource="<?php echo get_template_directory_uri(); ?>/images/thumbnail__BKA2501.jpg?itok=j4P1lLty" rel="og:image rdfs:seeAlso" class="field-item even"><a href="/articles/port-macquarie-%E2%80%93-grand-final-round"><img width="275" height="171" alt="" src="<?php echo get_template_directory_uri(); ?>/images/thumbnail__BKA2501.jpg?itok=j4P1lLty" typeof="foaf:Image"></a></div></div></div><div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/738/title/und/teaser"><div class="field-item"><a href="/articles/port-macquarie-%E2%80%93-grand-final-round">PORT MACQUARIE &ndash; THE GRAND FINAL ROUND</a></div></div></h2></div></div></div><div data-edit-id="node/738/body/und/teaser" class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even"><p>Expect plenty of speed, spray and action when a record number of teams compete at The Grand Final Round of the Australian Formula Powerboat Grand Prix Series (AFPGP) at Port Macquarie this weekend. There has been much anticipation and excitement leading up to this event.</p></div></div></div><div class="field field-name-node-link field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/articles/port-macquarie-%E2%80%93-grand-final-round">Read more</a></div></div></div></div>

  </div>
  <div class="views-row views-row-3 views-row-odd">
    <div class="ds-1col node node-article node-promoted node-teaser odd  view-mode-teaser clearfix" typeof="sioc:Item foaf:Document" about="/articles/grafton-round-3-%E2%80%93-great-weekend-racing">

  
  <div data-edit-id="node/736/field_image/und/teaser" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div resource="<?php echo get_template_directory_uri(); ?>/images/13558623_10154698919877923_5579786600696899017_o.jpg?itok=Zm8KaB-T" rel="og:image rdfs:seeAlso" class="field-item even"><a href="/articles/grafton-round-3-%E2%80%93-great-weekend-racing"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/13558623_10154698919877923_5579786600696899017_o.jpg?itok=Zm8KaB-T" typeof="foaf:Image"></a></div></div></div><div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/736/title/und/teaser"><div class="field-item"><a href="/articles/grafton-round-3-%E2%80%93-great-weekend-racing">GRAFTON ROUND 3 &ndash; GREAT WEEKEND OF RACING</a></div></div></h2></div></div></div><div data-edit-id="node/736/body/und/teaser" class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even"><p>There was an action packed program of powerboat racing on the Clarence River, Grafton over the weekend of June 25 &amp; 26 when teams from four Australian states and an international competitor from New Zealand competed at the third round of the Australian Formula Powerboat Grand Prix Series.</p></div></div></div><div class="field field-name-node-link field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/articles/grafton-round-3-%E2%80%93-great-weekend-racing">Read more</a></div></div></div></div>

  </div>
  <div class="views-row views-row-4 views-row-even">
    <div class="ds-1col node node-article node-promoted node-teaser even  view-mode-teaser clearfix" typeof="sioc:Item foaf:Document" about="/articles/grafton-anticipation-building-round-3">

  
  <div data-edit-id="node/732/field_image/und/teaser" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div resource="<?php echo get_template_directory_uri(); ?>/images/_BKA3394.jpg?itok=VwSTz4qJ" rel="og:image rdfs:seeAlso" class="field-item even"><a href="/articles/grafton-anticipation-building-round-3"><img width="275" height="195" alt="" src="<?php echo get_template_directory_uri(); ?>/images/_BKA3394.jpg?itok=VwSTz4qJ" typeof="foaf:Image"></a></div></div></div><div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/732/title/und/teaser"><div class="field-item"><a href="/articles/grafton-anticipation-building-round-3">GRAFTON: ANTICIPATION BUILDING FOR ROUND 3</a></div></div></h2></div></div></div><div data-edit-id="node/732/body/und/teaser" class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even"><p>If previous rounds of the 2016 season are anything to go, spectators at Corcoran Park, Grafton this weekend for Round 3 of the Australian Formula Powerboat Grand Prix Series will witness some top quality powerboat action along with plenty of thrills and spills.</p></div></div></div><div class="field field-name-node-link field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/articles/grafton-anticipation-building-round-3">Read more</a></div></div></div></div>

  </div>
  <div class="views-row views-row-5 views-row-odd views-row-last">
    <div class="ds-1col node node-article node-promoted node-teaser odd  view-mode-teaser clearfix" typeof="sioc:Item foaf:Document" about="/articles/preview-round-2-griffith">

  
  <div data-edit-id="node/725/field_image/und/teaser" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items"><div resource="<?php echo get_template_directory_uri(); ?>/images/_BKA1465%20%281280x945%29.jpg?itok=naBK04Jt" rel="og:image rdfs:seeAlso" class="field-item even"><a href="/articles/preview-round-2-griffith"><img width="275" height="203" alt="" src="<?php echo get_template_directory_uri(); ?>/images/_BKA1465%20%281280x945%29.jpg?itok=naBK04Jt" typeof="foaf:Image"></a></div></div></div><div class="field field-name-title field-type-ds field-label-hidden"><div class="field-items"><div property="dc:title" class="field-item even"><h2><div data-edit-id="node/725/title/und/teaser"><div class="field-item"><a href="/articles/preview-round-2-griffith">PREVIEW - ROUND 2 GRIFFITH</a></div></div></h2></div></div></div><div data-edit-id="node/725/body/und/teaser" class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even"><p>Following a successful start to the Formula Powerboat season at Lake Mulwala leading powerboat teams, their support crews, sponsors and supporters from around Australia are heading to Griffith for Round 2 of the Australian Formula Powerboat Grand Prix Series (AFPGP) at Lake Wyangan this weekend, April 16 and 17.</p></div></div></div><div class="field field-name-node-link field-type-ds field-label-hidden"><div class="field-items"><div class="field-item even"><a href="/articles/preview-round-2-griffith">Read more</a></div></div></div></div>

  </div>
-->

    </div>
  
      <!--<h2 class="element-invisible">Pages</h2>--><div class="item-list"> <!--<ul class="pager"><li class="pager-current first">1</li>
<li class="pager-item"><a href="/latest-news?page=1" title="Go to page 2">2</a></li>
<li class="pager-item"><a href="/latest-news?page=2" title="Go to page 3">3</a></li>
<li class="pager-item"><a href="/latest-news?page=3" title="Go to page 4">4</a></li>
<li class="pager-item"><a href="/latest-news?page=4" title="Go to page 5">5</a></li>
<li class="pager-item"><a href="/latest-news?page=5" title="Go to page 6">6</a></li>
<li class="pager-item"><a href="/latest-news?page=6" title="Go to page 7">7</a></li>
<li class="pager-item"><a href="/latest-news?page=7" title="Go to page 8">8</a></li>
<li class="pager-item"><a href="/latest-news?page=8" title="Go to page 9">9</a></li>
<li class="pager-ellipsis">…</li>
<li class="pager-next"><a href="/latest-news?page=1" title="Go to next page">next ›</a></li>
<li class="pager-last last"><a href="/latest-news?page=10" title="Go to last page">last »</a></li>
</ul>-->  
<?php wp_pagenavi(); ?>
</div>  
  
<?php
    /* Restore original Post Data */
  wp_reset_postdata();
} else {
  // no posts found
}

?>
  
  
</div>    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /content-inner -->
  </div><!-- /content -->
                                                      </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->
                                      </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div>

            
<?php get_footer(); ?>