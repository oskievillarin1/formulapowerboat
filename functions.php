<?php

function formula_setup() {
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( "title-tag" );	 //4.1

	$custom_back_args = array(
		'default-color' => 'EBEBEB',
	);
	add_theme_support( 'custom-background', $custom_back_args );	

	/*add_image_size('someblog-home-image',700,467,true);
	add_image_size('someblog-slide-image',1920,1080,true);	
	add_image_size('someblog-single-slide', 1140, 550, true);
	add_image_size('someblog-home-grid2', 505, 284, true);*/
	add_image_size('formula-latest-news', 275, 155, true);

	register_nav_menu('header-menu','Header Menu');
}

add_action( 'after_setup_theme', 'formula_setup' );


function formula_randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}


function generate_random_num() {
	$a = mt_rand(10000000,99999999); 
	return $a;
}


function formula_check_username() {
	$username = $_POST['check_username'];
	$email = $_POST['check_email'];

	if ( username_exists( $username ) ) {
		echo 'false';
	} else {

		if( email_exists( $email )) {
			echo 'false2';
		} else {
			echo 'true';
		}
	}

	exit();
}
add_action('wp_ajax_f_check_user', 'formula_check_username');
add_action('wp_ajax_nopriv_f_check_user', 'formula_check_username');//for users that are not logged in.

function formula_store_data() {

	global $wpdb;

	//checker
	$member_type_signup = $_POST['member_type_signup'];

	$rand_num = $_POST['random_number'];
	$m_name = $_POST['m_name'];
	$m_dob = $_POST['m_dob'];
	$m_res_address = $_POST['m_res_address'];
	$m_mailing_address = $_POST['m_mailing_address'];
	$m_home_phone = $_POST['m_home_phone'];
	$m_work_phone = $_POST['m_work_phone'];
	$m_mobile_phone = $_POST['m_mobile_phone'];
	$m_fax = $_POST['m_fax'];
	$m_email = $_POST['m_email'];
	$m_username = $_POST['m_username'];
	$member_or_assoc = $_POST['member_or_assoc'];
	$not_a_member = $_POST['not_a_member'];
	$member_or_assoc_name = $_POST['member_or_assoc_name'];
	$consent_name_photo = $_POST['consent_name_photo'];
	$member_sig = $_POST['member_sig'];
	$date_signed = $_POST['date_signed'];
	$mem_fee = $_POST['mem_fee'];


	//junior fields
	$g_name = $_POST['g_name'];
	$g_dob = $_POST['g_dob'];
	$g_res_address = $_POST['g_res_address'];
	$g_mailing_address = $_POST['g_mailing_address'];
	$g_home_phone = $_POST['g_home_phone'];
	$g_work_phone = $_POST['g_work_phone'];
	$g_mobile_phone = $_POST['g_mobile_phone'];
	$g_fax = $_POST['g_fax'];
	$g_email = $_POST['g_email'];
	$g_name_and_sig = $_POST['g_name_and_sig'];
	$m_name_and_sig = $_POST['m_name_and_sig'];


	//silver fields
	$member_type = $_POST['member_type'];

	//gold fields
	$boat_class = $_POST['boat_class'];
	$boat_owner = $_POST['boat_owner'];
	$boat_number = $_POST['boat_number'];
	$boat_name = $_POST['boat_name'];
	$licensed_driver = $_POST['licensed_driver'];

	$store_qry = "INSERT INTO " . $wpdb->prefix . "pending_users_data (
			reg_name,
			dob,
			res_address,
			mailing_address,
			home_phone,
			work_phone,
			mobile_phone,
			fax,
			email,
			team_associated,
			team_associated_name,
			website_content,
			member_signature,
			date_signed,
			join_as,
			boat_class,
			owner_of_boat,
			boat_number,
			boat_name,
			owner_licensed,
			member_type_signup,
			g_name,
			g_dob,
			g_res_address,
			g_mailing_address,
			g_home_phone,
			g_work_phone,
			g_mobile_phone,
			g_fax,
			g_email,
			g_name_and_sig,
			m_name_and_sig,
			random_id) VALUES (
				'" . $m_name . "',
				'" . $m_dob . "',
				'" . $m_res_address . "',
				'" . $m_mailing_address . "',
				'" . $m_home_phone . "',
				'" . $m_work_phone . "',
				'" . $m_mobile_phone . "',
				'" . $m_fax . "',
				'" . $m_email . "',
				'" . $member_or_assoc . "',
				'" . $member_or_assoc_name . "',
				'" . $consent_name_photo . "',
				'" . $member_sig . "',
				'" . $date_signed . "',
				'" . $member_type . "', 
				'" . $boat_class . "',
				'" . $boat_owner . "', 
				'" . $boat_number . "', 
				'" . $boat_name . "', 
				'" . $licensed_driver . "',
				'" . $member_type_signup . "',
				'" . $g_name . "',
				'" . $g_dob . "',
				'" . $g_res_address . "',
				'" . $g_mailing_address . "',
				'" . $g_home_phone . "',
				'" . $g_work_phone . "',
				'" . $g_mobile_phone . "',
				'" . $g_fax . "',
				'" . $g_email . "',
				'" . $g_name_and_sig . "',
				'" . $m_name_and_sig . "',
				'" . $rand_num . "'
			)
		";

	if($wpdb->query($store_qry)) {
		echo 'success';
	} else {
		echo $wpdb->last_error;
	}

	exit();
}
add_action('wp_ajax_f_store_data', 'formula_store_data');
add_action('wp_ajax_nopriv_f_store_data', 'formula_store_data');//for users that are not logged in.


function driver_post_type_coupon() {

	$labels = array (
		'name'   			 => _x('Drivers', 'Post Type General Name'),
	    'singular_name'      => _x('Driver', 'Post Type Singular Name'),
	    'menu_name'          => __('Drivers'),
	    'all_items'          => __('All Drivers'),
        'add_new'            => __('Add New')
		);

	 $args = array(
	        'label'               => __( 'driver'),
	        'description'         => __( 'The profile coupons separed by category'),
	        'labels'              => $labels,
	        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail' ),
	        'hierarchical'        => false,
	        'public'              => true
	        );

	register_post_type('driver', $args);

} 

add_action( 'init', 'driver_post_type_coupon', 0 );


function formulapower_scripts() {	
	wp_enqueue_style( 'formula-style', get_stylesheet_uri() );
	
}

add_action( 'wp_enqueue_scripts', 'formulapower_scripts' );


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	/*
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	*/
	
}


function formula_get_excerpt($num_chars) {
    $temp_str = substr(strip_shortcodes(strip_tags(get_the_content())),0,$num_chars);
    $temp_parts = explode(" ",$temp_str);
    $temp_parts[(count($temp_parts) - 1)] = '';
    
    if(strlen(strip_tags(get_the_content())) > 125)
      return implode(" ",$temp_parts) . '[...]';
    else
      return implode(" ",$temp_parts);
}

?>