<?php get_header(); ?>

<div class="main-wrapper full-width clearfix" id="main-wrapper">
        <div class="main region grid16-16" id="main">
          <div class="main-inner inner clearfix" id="main-inner">
            
            <!-- main group: width = grid_width - sidebar_first_width -->
            <div class="main-group region nested grid16-16" id="main-group">
              <div class="main-group-inner inner" id="main-group-inner">
                
                <div class="main-content region nested" id="main-content">
                  <div class="main-content-inner inner" id="main-content-inner">
                    <!-- content group: width = grid_width - sidebar_first_width - sidebar_second_width -->
                    <div class="content-group region nested grid16-16" id="content-group">
                      <div class="content-group-inner inner" id="content-group-inner">
                                                
                        <div class="content-region region nested" id="content-region">
                          <div class="content-region-inner inner" id="content-region-inner">
                            <a id="main-content-area"></a>

        <?php /* Start the Loop */ ?>
        <?php while ( have_posts() ) : the_post(); ?>     


              <h1 class="title gutter"><div data-edit-id="node/12/title/und/full"><div class="field-item"><?php the_title(); ?></div></div></h1>
                                                                                                                                              
<!-- content region -->
  <div class="region region-content content nested grid16-16" id="content">
    <div class="content-inner inner" id="content-inner">
      <div class="block block-system first last odd" id="block-system-main">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="ds-2col node node-page odd full-node view-mode-full clearfix" typeof="foaf:Document" about="/about-formula-powerboat-grand-prix">

  
  <div class="group-left">
    <div data-edit-id="node/12/body/und/full" class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even"><div class="info-box">

  

            <?php the_content(); ?>

       

</div>

</div></div></div>  </div>

 <?php endwhile; ?>

  <div class="group-right">
      <div data-edit-id="node/12/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items">

      <?php $odd_even = ''; ?>
      <?php $oe_counter = 0; ?>
      <?php if( have_rows('sidebar_images', 'option') ): ?>
          <?php while( have_rows('sidebar_images', 'option') ): the_row(); 
            if($oe_counter % 2 == 1) {
              $odd_even = 'odd';
            } else {
              $odd_even = 'even';
            }
            ?>
            <div class="field-item <?php echo $odd_even; ?>"><img style="max-width: 100%; height: auto;" alt="image" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></div>
            <?php $oe_counter++; ?>
          <?php endwhile; ?>
      <?php endif; ?>        

      <!--
      <div class="field-item even"><img width="275" height="184" alt="Formula Powerboat Racing" src="<?php echo get_template_directory_uri(); ?>/images/About%201.jpg?itok=12OOHWzZ" typeof="foaf:Image"></div>

      <div class="field-item odd"><img width="275" height="183" alt="Australian Powerboats" src="<?php echo get_template_directory_uri(); ?>/images/About%202.jpg?itok=v2ueaa-D" typeof="foaf:Image"></div>

      <div class="field-item even"><img width="275" height="183" alt="Powerboat Strategies" src="<?php echo get_template_directory_uri(); ?>/images/About%203.jpg?itok=S8cqVP9u" typeof="foaf:Image"></div>

      <div class="field-item odd"><img width="275" height="183" alt="Powerboat Sponsers" src="<?php echo get_template_directory_uri(); ?>/images/About%205.jpg?itok=p0xRBXWx" typeof="foaf:Image"></div>

      <div class="field-item even"><img width="275" height="184" alt="Powerboat" src="<?php echo get_template_directory_uri(); ?>/images/About%204.jpg?itok=OXgR2JLk" typeof="foaf:Image"></div>

      <div class="field-item odd"><img width="275" height="183" alt="Buoys" src="<?php echo get_template_directory_uri(); ?>/images/About%208.jpg?itok=zYkL3VGA" typeof="foaf:Image"></div>

      <div class="field-item even"><img width="275" height="183" alt="Powerboat Drivers" src="<?php echo get_template_directory_uri(); ?>/images/About%206.jpg?itok=IH5RTtKZ" typeof="foaf:Image"></div>

      <div class="field-item odd"><img width="275" height="184" alt="Boats Racing" src="<?php echo get_template_directory_uri(); ?>/images/About%207.jpg?itok=ECTGsiJ7" typeof="foaf:Image"></div>
      -->

    </div></div>  

  </div>

</div>

    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /content-inner -->
  </div><!-- /content -->
                                                      </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->
                                      </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div>

            
<?php get_footer(); ?>