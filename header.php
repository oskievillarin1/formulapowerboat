<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr" 
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr" 
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr" 
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9 ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr" 
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr" 
  xmlns:content="http://purl.org/rss/1.0/modules/content/"
  xmlns:dc="http://purl.org/dc/terms/"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:og="http://ogp.me/ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:sioc="http://rdfs.org/sioc/ns#"
  xmlns:sioct="http://rdfs.org/sioc/types#"
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema#"> <!--<![endif]-->

<head profile="http://www.w3.org/1999/xhtml/vocab">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="http://www.formulapowerboats.com.au/sites/all/themes/fusion_powerboats/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="shortlink" href="/node/81" />
<link rel="canonical" href="/formula-powerboats" />
  <!--<title>Formula Powerboats | Formula Powerboats Racing</title>-->
 <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/css_xE-rWrJf-fncB6ztZfd2huxqgxu4WO-qwma6Xer30m4.css" media="all" />
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/css_5YbdHr5Ydl82DxADBrSxdn1QRG2JoYejHCFqvNm0E3w.css" media="all" />
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css" media="all" />
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/datepicker.css" media="all" />
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css" media="all" />
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/css__eR_mHzskDXTa91FTEYW4JIvL4ZZsbItQSCnUlcfIns.css" media="all" /> 
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slicknav.css" media="all" />
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" media="all" />
<?php wp_head(); ?>
<style type="text/css" media="all">
<!--/*--><![CDATA[/*><!--*/
.countdownHolder{font-size:28px}

/*]]>*/-->
</style>
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/css_kEkfjtzuVZ3B4uc18M5k-NeIIBhRKaZ02gwxwCTu_1I.css" media="all" />
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/css_F2F_vhoootZelTX8bUT5CXwDcR78tlmkmd_Q-GoG_rw.css" media="all" />
<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.signature.css" media="all" />
<link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/south-street/jquery-ui.css" rel="stylesheet"> 
<!--    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.js?v=1.4.4"></script>-->
<!--    <script type="text/javascript" src="http://www.formulapowerboats.com.au/sites/all/modules/quicktabs/js/quicktabs.js?no5ozt"></script>-->

<!--<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.once.js?v=1.2"></script>-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/drupal.js?no5ozt"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/galleria-1.2.9.min.js?no5ozt"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/panels.js?no5ozt"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/views_slideshow.js?no5ozt"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/views_slideshow_galleria.js?no5ozt"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.signature.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/date.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/datepicker.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.slicknav.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script2.js?no5ozt"></script>
<?php $custom_path = '\/wpress_temp\/wp-content\/themes\/formula-power\/'; ?>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
/*
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"fusion_powerboats","theme_token":"hFwmKXMwNZIueVGPU5_5kxUZL_CWmpZIeWG8ckb7hcM","js":{"<?php echo $custom_path; ?>js\/jquery_countdown_timer.js":1,"<?php echo $custom_path; ?>js\/jquery_countdown_timer_init.js":1,"<?php echo $custom_path; ?>js\/jquery.js":1,"<?php echo $custom_path; ?>js\/jquery.once.js":1,"<?php echo $custom_path; ?>js\/drupal.js":1,"<?php echo $custom_path; ?>js\/galleria-1.2.9.min.js":1,"<?php echo $custom_path; ?>js\/panels.js":1,"<?php echo $custom_path; ?>js\/views_slideshow.js":1,"<?php echo $custom_path; ?>js\/views_slideshow_galleria.js":1,"<?php echo $custom_path; ?>js\/script.js":1},"css":{"<?php echo $custom_path; ?>css\/system.base.css":1,"<?php echo $custom_path; ?>css\/system.menus.css":1,"<?php echo $custom_path; ?>css\/system.messages.css":1,"<?php echo $custom_path; ?>css\/system.theme.css":1,"<?php echo $custom_path; ?>css\/comment.css":1,"<?php echo $custom_path; ?>css\/date.css":1,"<?php echo $custom_path; ?>css\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/all\/modules\/views\/css\/views.css":1,"sites\/all\/modules\/ckeditor\/css\/ckeditor.css":1,"sites\/all\/modules\/ctools\/css\/ctools.css":1,"sites\/all\/modules\/panels\/css\/panels.css":1,"sites\/all\/modules\/views_slideshow\/views_slideshow.css":1,"sites\/all\/modules\/ds\/layouts\/ds_2col_fluid\/ds_2col_fluid.css":1,"sites\/all\/modules\/ds\/layouts\/ds_2col\/ds_2col.css":1,"sites\/all\/modules\/views_slideshow_galleria\/views_slideshow_galleria.css":1,"sites\/all\/modules\/jquery_countdown_timer\/css\/jquery_countdown_timer.css":1,"0":1,"sites\/all\/themes\/fusion\/fusion_core\/css\/fusion-style.css":1,"sites\/all\/themes\/fusion\/fusion_core\/css\/fusion-typography.css":1,"sites\/all\/themes\/fusion\/fusion_core\/skins\/core\/fusion-core-skins.css":1,"sites\/all\/themes\/fusion_powerboats\/css\/fusion-powerboats-style.css":1,"sites\/all\/themes\/fusion\/fusion_core\/css\/grid16-960.css":1,"sites\/all\/themes\/fusion_powerboats\/css\/local.css":1,"public:\/\/css_injector\/css_injector_1.css":1}},"viewsSlideshow":{"gallery_block-block":{"methods":{"goToSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowGalleria"],"nextSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowGalleria"],"pause":["viewsSlideshowControls"],"play":["viewsSlideshowControls"],"previousSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowGalleria"],"transitionBegin":["viewsSlideshowPager","viewsSlideshowSlideCounter"],"transitionEnd":[]},"paused":0},"gallery_block-block_2":{"methods":{"goToSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowGalleria"],"nextSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowGalleria"],"pause":["viewsSlideshowControls"],"play":["viewsSlideshowControls"],"previousSlide":["viewsSlideshowPager","viewsSlideshowSlideCounter","viewsSlideshowGalleria"],"transitionBegin":["viewsSlideshowPager","viewsSlideshowSlideCounter"],"transitionEnd":[]},"paused":0}},"viewsSlideshowGalleria":{"views-slideshow-galleria-images-1":{"autoplay":2000,"carousel":true,"carouselFollow":true,"carouselSpeed":200,"carouselSteps":"auto","clicknext":true,"debug":true,"dummy":"","easing":"galleria","fullscreenCrop":false,"fullscreenDoubleTap":false,"fullscreenTransition":false,"height":340,"idleMode":true,"idleTime":3000,"imageCrop":"width","imageMargin":0,"imagePan":false,"imagePanSmoothness":12,"imagePosition":"center","keepSource":false,"layerFollow":true,"lightbox":false,"lightboxFadeSpeed":0,"lightboxTransitionSpeed":0,"maxScaleRatio":0,"minScaleRatio":0,"overlayOpacity":0.85,"overlayBackground":"#0b0b0b","pauseOnInteraction":true,"popupLlinks":false,"preload":2,"queue":true,"responsive":false,"show":0,"showInfo":true,"showCounter":false,"showImagenav":true,"swipe":true,"thumbCrop":"width","thumbFit":true,"thumbMargin":0,"thumbQuality":false,"thumbnails":true,"touchTransition":"fade","transition":"fade","transitionSpeed":800,"width":"auto","themePath":"<?php echo $custom_path; ?>js\/galleria.twelve.js"},"views-slideshow-galleria-images-2":{"autoplay":2000,"carousel":true,"carouselFollow":true,"carouselSpeed":200,"carouselSteps":"auto","clicknext":true,"debug":true,"dummy":"","easing":"galleria","fullscreenCrop":false,"fullscreenDoubleTap":false,"fullscreenTransition":false,"height":340,"idleMode":true,"idleTime":3000,"imageCrop":"width","imageMargin":0,"imagePan":false,"imagePanSmoothness":12,"imagePosition":"center","keepSource":false,"layerFollow":true,"lightbox":false,"lightboxFadeSpeed":0,"lightboxTransitionSpeed":0,"maxScaleRatio":0,"minScaleRatio":0,"overlayOpacity":0.85,"overlayBackground":"#0b0b0b","pauseOnInteraction":true,"popupLlinks":false,"preload":2,"queue":true,"responsive":false,"show":0,"showInfo":true,"showCounter":false,"showImagenav":true,"swipe":true,"thumbCrop":"width","thumbFit":true,"thumbMargin":0,"thumbQuality":false,"thumbnails":true,"touchTransition":"fade","transition":"fade","transitionSpeed":800,"width":"auto"}},"jquery_countdown_timer":{"jquery_countdown_timer_date":1470438000}});*/
//--><!]]>
</script>

<style type="text/css">
.2017_cal_cont .view-content {  border-left: 1px solid #c7c7c7; border-right: 1px solid #c7c7c7; border-bottom: 1px solid #c7c7c7; }
.2017_cal_cont .view-content .views-row.gradiented, 
.2017_cal_cont .view-content tr.gradiented { border-left: none; border-right: none; }
</style>

</head>

<!-- class="html front not-logged-in one-sidebar sidebar-second page-node page-node- page-node-81 node-type-page font-size-14 grid-type-960 grid-width-16 sidebars-split " -->
<?php
$extra_class = array( 'html', 'front', 'not-logged-in', 'one-sidebar', 'sidebar-second', 'page-node', 'page-node-', 'page-node-81', 'node-type-page', 'font-size-14', 'grid-type-960', 'grid-width-16', 'sidebars-split' );
?>

<body id="pid-formula-powerboats" <?php body_class($extra_class); ?> >
  <div id="skip-link">
    <a href="#main-content-area">Skip to main content area</a>
  </div>
  
<!-- page-top region -->
  <div id="page-top" class="region region-page-top page-top">
    <div id="page-top-inner" class="page-top-inner inner">
          </div><!-- /page-top-inner -->
  </div><!-- /page-top -->
  
  <div id="page" class="page">
    <div id="page-inner" class="page-inner">
      
      <!-- header-group region: width = grid_width -->
      <div id="header-group-wrapper" class="header-group-wrapper full-width clearfix">
        <div id="header-group" class="header-group region grid16-16">
          <div id="header-group-inner" class="header-group-inner inner clearfix">

                        <div id="header-site-info" class="header-site-info clearfix">
              <div id="header-site-info-inner" class="header-site-info-inner gutter">
                                <div id="logo">
                  <a href="<?php bloginfo('url'); ?>" title="Home"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Home" /></a>
                </div>
                                              </div><!-- /header-site-info-inner -->
            </div><!-- /header-site-info -->
            
            <div id="block-block-8" class="block block-block first last odd">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <!--<p style="text-align: center; color: #fff; text-transform: uppercase; font-size: 12px; font-weight: 800; letter-spacing: 8px; text-shadow: 1px 1px 3px #000; line-height: 1em; margin: 3px 0 0 0;"><br />Series Partner</p>-->

      <div class="mem_login_cont2">
        <a href="http://afpgp.com.au/become-a-member/"><img src="<?php echo get_template_directory_uri(); ?>/images/Formula-Button-Become.png" alt="member" /></a>
          <br />

          <a href="<?php bloginfo('url'); ?>/login/"><img src="<?php echo get_template_directory_uri(); ?>/images/members-login-button.png" alt="login" /></a>
      </div>

    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
                      </div><!-- /header-group-inner -->
        </div><!-- /header-group -->
      </div><!-- /header-group-wrapper -->

      
<!-- preface-top region -->
<div id="preface-top-wrapper" class="preface-top-wrapper full-width clearfix">
  <div id="preface-top" class="region region-preface-top preface-top  grid16-16">
    <div id="preface-top-inner" class="preface-top-inner inner">
      <div id="block-system-main-menu" class="block block-system block-menu first last odd">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <!--
      <ul class="menu"><li class="first leaf"><a href="/" class="active">Home</a></li>
<li class="leaf"><a href="/latest-news">Latest News</a></li>
<li class="leaf"><a href="/about-formula-powerboat-grand-prix">About</a></li>
<li class="leaf"><a href="/divisions">Drivers</a></li>
<li class="leaf"><a href="/results">Results</a></li>
<li class="leaf"><a href="/sponsorship">Sponsorship</a></li>
<li class="leaf"><a href="/galleries">Galleries</a></li>
<li class="leaf"><a href="/videos">Video</a></li>
<li class="leaf"><a href="/forms">Forms</a></li>
<li class="last leaf"><a href="/contact-us">Contact</a></li>
</ul>-->    

  <?php wp_nav_menu('theme_location=header-menu&container=false&menu_class=menu&menu_id=the_menu'); ?>
</div>
<div class="mobile_menu">
  <a href="#">MENU</a>
</div>
  </div><!-- /block-inner -->
</div><!-- /block -->
<div class="stopper"></div>
    </div><!-- /preface-top-inner -->
  </div><!-- /preface-top -->
</div><!-- /preface-top-wrapper -->