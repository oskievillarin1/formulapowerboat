<?php
/*
  Template Name: Bronze Test Form
*/
?>
<?php get_header(); ?>
<?php

if($_GET['test'] == 'yes') {

              $op["op"] = "create_user"; // The Remote Operation.

              $op["api_key"] = "e46c09420ca2038ad1e241278d7ac029"; // Check your Dashboard for this value.
                  // See: `s2Member → API / Scripting → Remote Operations API → API Key`

              $op["data"] = array(
                  "user_login" => "bryanking", // Required. A unique Username. Lowercase alphanumerics/underscores.
                  "user_email" => "bryanjosephking@gmail.com", // Required. A valid/unique Email Address for the new User.

                  // These additional details are 100% completely optional.

                  "modify_if_login_exists" => "1", // Optional. Update/modify if ``user_login`` value already exists in the database?
                      // A non-zero value tells s2Member to update/modify an existing account with the details you provide, if this Username already exists.

                  //"user_pass" => "456DkaIjsd!", // Optional. Plain text Password. If empty, this will be auto-generated.

                  "first_name" => "John", // Optional. First Name for the new User.
                  "last_name" => "Doe", // Optional. Last Name for the new User.

                  "s2member_level" => "2", // Optional. Defaults to Level #0 (a Free Subscriber).
                  //"s2member_ccaps" => "music,videos", // Optional. Comma-delimited list of Custom Capabilities.

                  //"s2member_registration_ip" => "123.456.789.100", // Optional. User's IP Address. If empty, s2Member will fill this upon first login.

                  //"s2member_subscr_gateway" => "paypal", // Optional. User's Paid Subscr. Gateway Code. One of: (paypal|alipay|authnet|ccbill|clickbank|google).
                  //"s2member_subscr_id" => "I-DJASODJF8933J", // Optional. User's Paid Subscr. ID. For PayPal®, use their Subscription ID, or Recurring Profile ID.

                  //"s2member_custom" => "afpgp.com.au", // Optional. If provided, should always start with your installation domain name (i.e., $_SERVER["HTTP_HOST"]).

                  //"s2member_auto_eot_time" => "2030-12-25", // Optional. Can be any value that PHP's ``strtotime()`` function will understand (i.e., YYYY-MM-DD).

                  //"custom_fields" => array("my_field_id" => "Some value."), // Optional. An array of Custom Registration/Profile Field ID's, with associative values.

                  //"s2member_notes" => "Administrative notation. Created this User via API call.", // Optional. Administrative notations.

                  //"opt_in" => "1", // Optional. A non-zero value tells s2Member to attempt to process any List Servers you've configured in the Dashboard area.
                      // This may result in your mailing list provider sending the User/Member a subscription confirmation email (i.e., ... please confirm your subscription).

                  //"notification" => "1", // Optional. A non-zero value tells s2Member to email the new User/Member their Username/Password.
                      // The "notification" parameter also tells s2Member to notify the site Administrator about this new account.
              );

              $post_data = stream_context_create (array("http" => array("method" => "POST", "header" => "Content-type: application/x-www-form-urlencoded", "content" => "s2member_pro_remote_op=" . urlencode (serialize ($op)))));

              $result = trim (file_get_contents ("http://afpgp.com.au/?s2member_pro_remote_op=1", false, $post_data));

              if (!empty($result) && !preg_match ("/^Error\:/i", $result) && is_array($user = @unserialize ($result)))
                  echo "Success. New User created with ID: " . $user["ID"];
              else
                  echo "API error reads: " . $result;


}

?>

<div class="main-wrapper full-width clearfix" id="main-wrapper">
        <div class="main region grid16-16" id="main">
          <div class="main-inner inner clearfix" id="main-inner">
            
            <!-- main group: width = grid_width - sidebar_first_width -->
            <div class="main-group region nested grid16-16" id="main-group">
              <div class="main-group-inner inner" id="main-group-inner">
                
                <div class="main-content region nested" id="main-content">
                  <div class="main-content-inner inner" id="main-content-inner">
                    <!-- content group: width = grid_width - sidebar_first_width - sidebar_second_width -->
                    <div class="content-group region nested grid16-16" id="content-group">
                      <div class="content-group-inner inner" id="content-group-inner">
                                                
                        <div class="content-region region nested" id="content-region">
                          <div class="content-region-inner inner" id="content-region-inner">
                            <a id="main-content-area"></a>
                                                                                                                                            <h1 class="title gutter"><div data-edit-id="node/92/title/und/full"><div class="field-item">Forms</div></div></h1>
                                                                                                                                              
<!-- content region -->
  <div class="region region-content content nested grid16-16" id="content">
    <div class="content-inner inner" id="content-inner">
      <div class="block block-system first last odd" id="block-system-main">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="ds-2col node node-page odd full-node view-mode-full clearfix" typeof="foaf:Document" about="/forms">

  
  <div class="group-left">
    <div data-edit-id="node/92/body/und/full" class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even"><style type="text/css">
&lt;!--/*--&gt;&lt;![CDATA[/* &gt;&lt;!--*/
.content a { color: blue; }

/*--&gt;&lt;!]]&gt;*/
</style>



        <?php /* Start the Loop */ ?>
        <?php while ( have_posts() ) : the_post(); ?>       

            <?php //the_content(); ?>


            <h3>Member Details</h3>

            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="my_paypal_form" id="formula_paypal_form">
            <div class="fp_field_row">
              <label>Name:</label> <input type="text" name="m_name" id="m_name_id" />
            </div>

            <div class="fp_field_row">
              <label>Date of Birth:</label> <select name="dob_year" id="dob_year_id">
              <?php
              for($x=1920;$x<=2016;$x++) {
                echo '<option value="' . $x . '">' . $x . '</option>';
              }
              ?>
              </select> - <select name="dob_month" id="dob_month_id">
              <?php
              for($y=1;$y<=12;$y++) {
                if(strlen($y) == 1) { $y = '0' . $y; }
                echo '<option value="' . $y . '">' . $y . '</option>';
              }
              ?>
            </select> - <select name="dob_day" id="dob_day_id">
            <?php
            for($z=1;$z<=31;$z++) {
              if(strlen($z) == 1) { $z = '0' . $z; }
              echo '<option value="' . $z . '">' . $z . '</option>';
            }
            ?>
            </select> <!--<input type="text" name="m_dob" id="m_dob_id" class="date-pick" autocomplete="off" />-->
            <input type="hidden" name="m_dob" id="m_dob_id" />
            </div>

            <div class="fp_field_row">
              <label>Residential Address:</label> <input type="text" name="m_res_address" />
            </div>

            <div class="fp_field_row">
              <label>Mailing Address (if different from above):</label> <input type="text" name="m_mailing_address" />
            </div>

            <div class="fp_field_row">
              <label>Home Phone:</label> <input type="text" name="m_home_phone" />
            </div>

            <div class="fp_field_row">
              <label>Work:</label> <input type="text" name="m_work_phone" />
            </div>

            <div class="fp_field_row">
              <label>Mobile:</label> <input type="text" name="m_mobile_phone" />
            </div>

           <!-- <div class="fp_field_row">
              <label>Fax:</label> <input type="text" name="m_fax" />
            </div>-->

            <div class="fp_field_row">
              <label>Email:</label> <input type="text" name="m_email" id="m_email_id" />
            </div>

            <!--<div class="fp_field_row">
              <label>Username:</label> <input type="text" name="m_username" id="m_username_id" />
            </div>-->

            <br />
            <h3>Team Association</h3>

            <div class="fp_field_row">
              <input type="checkbox" value="member_or_assoc" name="member_or_assoc" /> I am a member or associated with the <input type="text" name="member_or_assoc_name" /> team that competes in the AFPGP
            </div>

            <div class="fp_field_row">
              <input type="checkbox" name="not_a_member" value="no" /> I am not a member of or associated with any particular team that competes in the AFPGP
            </div>

            <br />
            <h3>Website Consent – Committee Members only (please tick)</h3>

            <div class="fp_field_row">
              <input type="radio" name="consent_name_photo" value="i_do" /> If needed, I do consent to my name/photo to be placed on the AFPGP website in the relevant committee/contact areas
            </div>

            <div class="fp_field_row">
              <input type="radio" name="consent_name_photo" value="i_dont" /> I do not consent to my name/photo to be placed on the AFPGP website in the relevant committee/contact areas
            </div>

            <br />

            <!--<div class="fp_field_row">
              <label>Member Signature:<br />(use your mouse to sign below)</label> <input type="hidden" name="member_sig" /> <div class="sig_area" style="width: 400px; height: 80px;"></div>
            </div>     -->

            <div class="fp_field_row">
              Date: <input type="text" name="date_signed" />
            </div>     

            <br />
            <!--<h3>Membership Fee & Payment (please tick)</h3>-->

            <div class="fp_field_row">
              <!--<input type="checkbox" name="mem_fee" value="20" /> -->Membership fee <b>$20</b> (Pay securely by Credit Card / Paypal)
            </div>

            <!--<p>EFT : CBA AFPGP BSB: 064823 A/C: 10752878 Date paid:...........................Reference ......................................</p>

            <br />
            <h3>Submission of Membership Form:</h3>
            <p>Scan to: entriesandmemberships@formulapowerboats.com.au or<br />Post to: The Secretary AFPGP, PO BOX 352 Raymond Terrace NSW 2324</p>

            <div class="form_cont_with_border">
              <h4>OFFICIAL USE ONLY</h4>

              <p>Membership Number Issued ____________ &nbsp;&nbsp;&nbsp; Date Issued ____________</p>
              <p>Official’s Name & Signature  ________________________</p>
            </div>
            -->

            <div style="display: none;">
              <input type="hidden" name="cmd" value="_cart">
              <input type="hidden" name="upload" value="1" />
              <input type="hidden" name="business" value="president@formulapowerboats.com.au">
              <input type="hidden" name="return" value="http://afpgp.com.au/">
              <!-- <input type="hidden" name="cancel_return" value="">-->
              <input type="hidden" name="notify_url" value="http://afpgp.com.au/ipn/">
              <input type="hidden" name="currency_code" value="AUD">         

              <input type="hidden" name="item_number_1" value="1">
              <input type="hidden" name="item_name_1" value="Australian Formula Bronze Membership">
              <input type="hidden" name="amount_1" value="1">
              <input type="hidden" name="quantity_1" value="1">
              <input type="hidden" name="custom" id="custom_id" value="" />
              <!--<input type="hidden" id="pp_shipping' . $counter . '" name="shipping_' . $counter . '" value="' . $shipping_price_arr[$x] . '" />-->
              <input type="hidden" name="random_number" value="<?php echo generate_random_num(); ?>" id="random_number_id" />

            </div>

            <div class="fp_field_row fp_field_row_submit">
              <input type="hidden" name="member_type_signup" value="bronze" />
              <div style="display: none;" class="fp_field_loading"><p>Please wait....</p></div>
              <input type="submit" value="PAY NOW" id="formula_submit_id" />
            </div>
            </form>

        <?php endwhile; ?>

<!--
<div class="info-box">
  <h3>MEMBERSHIP FORMS</h3>

  <ul><li><a href="/sites/default/files/images/AFPGP%20Levels%20of%20Membership%28UPDATE%29_FINAL.pdf">Levels of Membership&nbsp;</a></li>
    <li><a href="/sites/default/files/pdf/MembershipForms/2016-Gold-Membership.pdf">2016&nbsp;GOLD&nbsp;Membership</a>&nbsp;(Licensed Driver)</li>
    <li><a href="/sites/default/files/pdf/MembershipForms/2016-Silver-Membership.pdf">2016&nbsp;SILVER&nbsp;Membership</a>&nbsp;(Boat Owners, Team Crew Chiefs,&nbsp;Committee Members &amp; FF Guardians)</li>
    <li><a href="/sites/default/files/pdf/MembershipForms/2016-Bronze-Membership.pdf">&#8203;2016&nbsp;BRONZE&nbsp;Membership</a>&nbsp;(Associate Members)</li>
    <li><a href="/sites/default/files/pdf/MembershipForms/2016-FF-Membership.pdf">2016 Formula Future Membership</a></li>
  </ul><h3>ENTRY FORMS:</h3>

  <ul><li><a href="/sites/default/files/pdf/EntryForms/Updated%20AFPGP%20Senior%20Entry%20Form%20port.docx">Round 4&nbsp;| PORT MACQUARIE&nbsp;- Driver Entry Form</a></li>
    <li><a href="/sites/default/files/pdf/EntryForms/Updated%20AFPGP%20Form%20Future%20Entry%20Form%20port.docx">Round 4&nbsp;| PORT MACQUARIE- Formula Future Entry Form</a></li>
  </ul><h3>EVENT NOTES:</h3>

  <ul><li><a href="/sites/default/files/pdf/EventNotes/Event%20Notes%20-%20Port%202016.docx">Round 4&nbsp;|&nbsp;PORT MACQUARIE</a></li>
  </ul><h3>OTHER:</h3>

  <ul><li><a href="/sites/default/files/pdf/AFPGP%202013%20-%202014%20Sponsorship%20Proposal%20-%20P.pdf">&#8203;</a><a href="/sites/default/files/pdf/AFPGP%202016%20Sponsorship%20Proposal%20Port%20V1%20small%20file%20size.pdf">AFPGP&nbsp;Sponsorship Proposal</a>&nbsp;</li>
    <li><a href="/sites/default/files/pdf/2015-16%20Licence%20Payment%20Methods.pdf">2015 / 2016 License Payment Methods</a></li>
    <li><a href="/sites/default/files/pdf/2015-16%20Licence%20Prices%20%26%20Information.pdf">2015 / 2016 Prices &amp; Information</a></li>
    <li><a href="/sites/default/files/pdf/2015-2016%20Race%20Calendar%20and%20NSW%20Executive.pdf">2015 / 2016 Race Calander &amp; NSW Executives</a></li>
    <li><a href="/sites/default/files/pdf/APBA%20Medical%20Form%202015-2016.docx">APBA Medical Form</a></li>
    <li><a href="/sites/default/files/pdf/Boat%20Owner%20Form%202015-2016.pdf">2015 / 2016 Boat Owner Form</a></li>
    <li><a href="/sites/default/files/pdf/Formula%20Future%20Licence%20%26%20Medical%202014-2015.pdf">2015 / 2016 Formula Future License &amp; Medical</a></li>
    <li><a href="/sites/default/files/pdf/Licence%20Form%202015-2016.pdf">2015 / 2016 License Form</a></li>
  </ul></div>

<div class="info-box">
  <h1>RULES</h1>

  <ul><li><a href="/sites/default/files/pdf/ClubRules/AFPGP%20Constitution%20-%20Adopted%202-11-13.pdf">Club Constitution</a>&nbsp;&nbsp; (effective 29 Nov 13)</li>
    <li><a href="/sites/default/files/images/2014%20AFPGP%20Series%20Race%20Rules%20v3_2014_11_12_FINAL%20DRAFT.pdf">AFPGP Series Racing Rules</a> (adopted 12 Nov 2014)</li>
    <li><a href="http://www.ausapba.com.au/download_index.htm">APBA Rule Book 2014</a></li>
  </ul><p><span style="line-height: 1.6em;">As an Incorporated Association, we are also governed by the following legislation:</span></p>

  <ul><li><a href="/sites/default/files/pdf/ClubRules/Associations%20Incorporation%20Act%202009.pdf">Associations Incorporation Act 2009</a></li>
    <li><a href="/sites/default/files/pdf/ClubRules/Associations%20Incorporation%20Regulation%202010.pdf">Associations Incorporation Regulation 2010</a></li>
  </ul></div>
</div>
-->

</div></div>  </div>

  <div class="group-right">
    <div data-edit-id="node/92/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items">


<?php

// check if the repeater field has rows of data
if( have_rows('forms_side_images') ):

  // loop through the rows of data
    while ( have_rows('forms_side_images') ) : the_row();

        // display a sub field value
        //the_sub_field('sub_field_name'); ?>
      <div class="field-item even about_side_img"><img alt="image" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></div>

<?php

    endwhile;

else :

    // no rows found

endif;

?>
<!--
      <div class="field-item even"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Forms%20Rules%201.jpg?itok=MTw3Uf8s" typeof="foaf:Image"></div><div class="field-item odd"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Forms%20Rules%202.jpg?itok=MUq6OFmC" typeof="foaf:Image"></div><div class="field-item even"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Forms%20Rules%204.jpg?itok=LDZiJqk3" typeof="foaf:Image"></div><div class="field-item odd"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/Forms%20Rules%205.jpg?itok=lpHscg9G" typeof="foaf:Image"></div><div class="field-item even"><img width="275" height="183" alt="" src="<?php echo get_template_directory_uri(); ?>/images/contact%20us%2012.jpg?itok=rvHT_CJL" typeof="foaf:Image"></div>
-->

    </div></div>  </div>

</div>

    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /content-inner -->
  </div><!-- /content -->
                                                      </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->
                                      </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div>
            
<?php get_footer(); ?>