<!-- footer region -->
<div id="footer-wrapper" class="footer-wrapper full-width clearfix">
  <div id="footer" class="region region-footer footer  grid16-16">
    <div id="footer-inner" class="footer-inner inner">
      <div id="block-block-3" class="block block-block first  odd">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <p><img alt="Formula Powerboats" src="<?php echo get_template_directory_uri(); ?>/images/Formula-Base-Logo.png" /></p>
      <p style="text-align: center; color: #fff;">Copyright © 2014. Australian Formula Powerboats. All Rights Reserved.</p>
    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
<div id="block-block-2" class="block block-block   even">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <p style="text-align: right;"><a href="http://www.facebook.com/pages/AUSTRALIAN-FORMULA-POWERBOAT-GRAND-PRIX/129912826572"><img alt="Find us on Facebook" src="<?php echo get_template_directory_uri(); ?>/images/Facebook-Logo.png" style="margin-top: 10px;" /></a><!--<img alt="Twitter" src="/sites/default/files/images/social-twitter.png" style="width: 73px; height: 60px; margin: 0 5px;" /><img alt="YouTube Channel" src="/sites/default/files/images/social-youtube.png" style="width: 73px; height: 60px;" />--></p>
    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
<div id="block-block-4" class="block block-block  last odd">
  <div class="gutter inner clearfix">
            <!--
    <div class="content clearfix">
      <p style="text-align: center;">Copyright © 2014. Australian Formula Powerboats. All Rights Reserved.</p>
    </div> -->
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /footer-inner -->
  </div><!-- /footer -->
</div><!-- /footer-wrapper -->
    </div><!-- /page-inner -->
  </div><!-- /page -->
  
<!-- page-bottom region -->
  <div id="page-bottom" class="region region-page-bottom page-bottom">
    <div id="page-bottom-inner" class="page-bottom-inner inner">
          </div><!-- /page-bottom-inner -->
  </div><!-- /page-bottom -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery_countdown_timer.js?no5ozt"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery_countdown_timer_init.js?no5ozt"></script>
<?php wp_footer(); ?>

<script>

jQuery(document).ready(function($) {

  $('#formula_standard_submit_id').click(function(e) {

    e.preventDefault();

    var error_msg = '';

    $('#formula_standard_submit_id').css('display','none');
    $('.fp_field_loading').css('display','block');

    $('#m_dob_id').val($('#dob_year_id').val() + '-' + $('#dob_month_id').val() + '-' + $('#dob_day_id').val());

    //if( $('#m_username_id').val() == '' ) {
    //  alert('Please input username');
    //} else 


    if($('#name_of_event_id').val() == '') {
      error_msg += '\n' + 'Please input event name';
    }

    if($('#event_class_id').val() == '') {
      error_msg += '\n' + 'Please input class';
    }

    if($('#date_of_event_id').val() == '') {
      error_msg += '\n' + 'Please input date of event';
    }

    if($('#event_venue_id').val() == '') {
      error_msg += '\n' + 'Please input event venue';
    }

    if($('#name_of_boat_id').val() == '') {
      error_msg += '\n' + 'Please input name of boat';
    }

    if($('#race_number_id').val() == '') {
      error_msg += '\n' + 'Please input race number';
    }

    if($('#sba_reg_num_id').val() == '') {
      error_msg += '\n' + 'Please input SBA Reg Num';
    }

    if(error_msg != '') {
      alert(error_msg);
      $('#formula_standard_submit_id').css('display','inline-block');
      $('.fp_field_loading').css('display','none');      
    } else {
      alert('success');
    }

    /*
    if(!validateEmail($('#m_email_id').val())) {
      alert('Please input a valid email.');

      $('#formula_submit_id').css('display','inline-block');
      $('.fp_field_loading').css('display','none');

    } else {

        $.ajax({
          url:"<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
          type:'POST',
          data:'action=f_check_user&check_username=' + $('#m_username_id').val() + '&check_email=' + $('#m_email_id').val(),

          success:function(results)
          {
            //  alert(results);
            console.log(results);
            if(results == 'false') {
              alert('Username already exists');
            } else if(results == 'false2') {
              alert('Email already exists');
            } else {


              $.ajax({
                url:"<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
                type:'POST',
                data:'action=f_store_data&' + $('#formula_paypal_form').serialize(),

                success:function(results)
                {
                  //$('#custom_id').val( $('#m_username_id').val() + '|||' + $('#m_email_id').val() + '|||' + $('#m_name_id').val() );

                  console.log(results);
                  $('#custom_id').val( $('#random_number_id').val() );
                  $('#formula_paypal_form').trigger('submit');  

                  $('#formula_submit_id').css('display','inline-block');
                  $('.fp_field_loading').css('display','none');

                }
              });


            
            }
          }
        });


    }
    */

  });

  $('#formula_submit_id').click(function(e) {
    e.preventDefault();

    $('#formula_submit_id').css('display','none');
    $('.fp_field_loading').css('display','block');

    $('#m_dob_id').val($('#dob_year_id').val() + '-' + $('#dob_month_id').val() + '-' + $('#dob_day_id').val());

    //if( $('#m_username_id').val() == '' ) {
    //  alert('Please input username');
    //} else 
    if(!validateEmail($('#m_email_id').val())) {
      alert('Please input a valid email.');
    } else {

        $.ajax({
          url:"<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
          type:'POST',
          data:'action=f_check_user&check_username=' + $('#m_username_id').val() + '&check_email=' + $('#m_email_id').val(),

          success:function(results)
          {
            //  alert(results);
            console.log(results);
            if(results == 'false') {
              alert('Username already exists');
            } else if(results == 'false2') {
              alert('Email already exists');
            } else {


              $.ajax({
                url:"<?php bloginfo('url'); ?>/wp-admin/admin-ajax.php",
                type:'POST',
                data:'action=f_store_data&' + $('#formula_paypal_form').serialize(),

                success:function(results)
                {
                  //$('#custom_id').val( $('#m_username_id').val() + '|||' + $('#m_email_id').val() + '|||' + $('#m_name_id').val() );

                  console.log(results);
                  $('#custom_id').val( $('#random_number_id').val() );
                  $('#formula_paypal_form').trigger('submit');  

                  $('#formula_submit_id').css('display','inline-block');
                  $('.fp_field_loading').css('display','none');

                }
              });


            
            }
          }
        });


    }


  });

});

</script>

</body>
</html>