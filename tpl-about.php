<?php
/*
  Template Name: About
*/
?>
<?php get_header(); ?>

<div class="main-wrapper full-width clearfix" id="main-wrapper">
        <div class="main region grid16-16" id="main">
          <div class="main-inner inner clearfix" id="main-inner">
            
            <!-- main group: width = grid_width - sidebar_first_width -->
            <div class="main-group region nested grid16-16" id="main-group">
              <div class="main-group-inner inner" id="main-group-inner">
                
                <div class="main-content region nested" id="main-content">
                  <div class="main-content-inner inner" id="main-content-inner">
                    <!-- content group: width = grid_width - sidebar_first_width - sidebar_second_width -->
                    <div class="content-group region nested grid16-16" id="content-group">
                      <div class="content-group-inner inner" id="content-group-inner">
                                                
                        <div class="content-region region nested" id="content-region">
                          <div class="content-region-inner inner" id="content-region-inner">
                            <a id="main-content-area"></a>
                                                                                                                                            <h1 class="title gutter"><div data-edit-id="node/12/title/und/full"><div class="field-item">About the Formula Powerboat Grand Prix</div></div></h1>
                                                                                                                                              
<!-- content region -->
  <div class="region region-content content nested grid16-16" id="content">
    <div class="content-inner inner" id="content-inner">
      <div class="block block-system first last odd" id="block-system-main">
  <div class="gutter inner clearfix">
            
    <div class="content clearfix">
      <div class="ds-2col node node-page odd full-node view-mode-full clearfix" typeof="foaf:Document" about="/about-formula-powerboat-grand-prix">

  
  <div class="group-left">
    <div data-edit-id="node/12/body/und/full" class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div property="content:encoded" class="field-item even"><div class="info-box">

        <?php /* Start the Loop */ ?>
        <?php while ( have_posts() ) : the_post(); ?>       

            <?php the_content(); ?>

        <?php endwhile; ?>

<!--  
  <p>Whilst Formula Powerboat racing commenced in Australia in 1988, it was in 1997 the teams recognised that an organisation was needed to manage and promote a National Series for professional teams from&nbsp;all states. With that, the Australian F1 Superboat Series was formed.</p>

  <p>In 2008 the promoter responsible for the F1 Superboat Series ceased operating. In September 2009&nbsp;a new “driver owned and run” Grand Prix came to life.&nbsp;&nbsp;</p>

  <p>The “Formula Powerboat Grand Prix” is a not for profit association formed to bring together top class competitors from around Australia to compete in the 5&nbsp;Round Series each year.&nbsp;Five&nbsp;classes of&nbsp;professional formula powerboats and Formula Futures - our junior drivers are catered for. Invitational classes are also run for&nbsp;Sports Monos, 6Ltr Inboard ProStocks, 550cc/25hp.</p>

  <p>Formula Powerboats continue to be associated with club venues in each state to assist in bringing drivers from the grass roots of the sport through to a professional level.</p>

  <p>Most Drivers have worked their way up to a Formula boat from club racing. Some started their careers&nbsp;in Monos (Basic Mono hull design boats through to more advanced modified MOD VP classes), these styles of boats are generally used in ski racing and more recently in oval race circuits such as Formula boats use.</p>

  <p>Formula powerboat racing is an extreme sport that is both fast and colourful. Explosive starts, hairpin turns and high speed passing maneuvers provide continuous action and excitement for spectators of all ages. Spectators can view the entire course from any vantage point, ensuring that they don’t miss any of the action.</p>
-->
</div>

<div class="info-box">
  <h1>THE COMMITTEE</h1>

  <div>
    <table cellspacing="1" cellpadding="4" border="0" align="left" style="width:100%;"><tbody><tr><td colspan="4">
            <h2 class="highlight">Executive Commitee</h2>
          </td>
        </tr>

<?php

// check if the repeater field has rows of data
if( have_rows('executive_commitee') ):

  // loop through the rows of data
    while ( have_rows('executive_commitee') ) : the_row();

        // display a sub field value
        //the_sub_field('sub_field_name'); ?>
        <tr><td>
            <p>&nbsp;</p>
          </td>
          <td>
            <p><strong><?php the_sub_field('name_left'); ?></strong><br><span style="line-height: 1.6em;"><?php the_sub_field('position_left'); ?></span><br><span style="line-height: 1.6em;"><?php the_sub_field('location_left'); ?></span></p>
          </td>
          <td>
            <p>&nbsp;</p>
          </td>
          <td>
            <p><b><?php the_sub_field('name_right'); ?></b><br><span style="line-height: 1.6em;"><?php the_sub_field('position_right'); ?></span><br><span style="line-height: 1.6em;"><?php the_sub_field('location_right'); ?></span></p>
          </td>
        </tr>
<?php

    endwhile;

else :

    // no rows found

endif;

?>




<!--
        <tr><td>
            <p>&nbsp;<img style="border-style:none; height:150px; margin:0px; width:100px" src="http://formulapowerboats.com.au/site_folders/59/images/Committee%20Photos/gavin%20simmons.jpg" alt=""></p>
          </td>
          <td>
            <p><strong>Gavin Simmons</strong><br><span style="line-height: 1.6em;">President</span><br><span style="line-height: 1.6em;">San Remo, NSW</span></p>
          </td>
          <td>
            <p>&nbsp;</p>
          </td>
          <td>
            <p><b>Glenn Burns</b><br>
              Vice President<br>
              Regents Park, NSW</p>
          </td>
        </tr>


        <tr><td>
            <p>&nbsp;</p>
          </td>
          <td>
            <p><strong>Robyn Cleary</strong><br><span style="line-height: 1.6em;">Treasurer</span><br><span style="line-height: 1.6em;">Carlton, VIC</span></p>
          </td>
          <td>
            <p>&nbsp;</p>
          </td>
          <td>
            <p><b>Danielle Martin</b><br><span style="line-height: 1.6em;">Secretary</span><br><span style="line-height: 1.6em;">Newcastle, NSW</span></p>
          </td>
        </tr>
-->

      </tbody></table><table cellspacing="1" cellpadding="4" border="0" align="left" style="width:100%;"><tbody><tr><td colspan="2">
            <h2 class="highlight">Other Committee Members</h2>
          </td>
        </tr><tr><td>
            <p><strong>&nbsp;POSITION</strong></p>
          </td>
          <td>
            <p><strong>&nbsp;NAME</strong></p>
          </td>
        </tr>

<?php

// check if the repeater field has rows of data
if( have_rows('other_committee_members') ):

  // loop through the rows of data
    while ( have_rows('other_committee_members') ) : the_row();

        // display a sub field value
        //the_sub_field('sub_field_name'); ?>
        <tr><td>
            <p>&nbsp;<?php the_sub_field('position'); ?></p>
          </td>
          <td>
            <p><?php the_sub_field('name'); ?></p>
          </td>
        </tr>
<?php

    endwhile;

else :

    // no rows found

endif;

?>

        <!--
        <tr><td>
            <p>&nbsp;Race Day Co-ordinator</p>
          </td>
          <td>
            <p>Gavin Simmons</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;Entries &amp; Memberships</p>
          </td>
          <td>
            <p>&nbsp;Rebecca Fowler</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;Minutes Secretary</p>
          </td>
          <td>
            <p>Rebecca Fowler&nbsp;</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;Rules Officer</p>
          </td>
          <td>
            <p>Don McClymont&nbsp;</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;Safety Officer</p>
          </td>
          <td>
            <p>&nbsp;Ron Beasley</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;Sponsorship Co-ordinator</p>
          </td>
          <td>
            <p>Lizzy Heaney&nbsp;</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;Media Officer</p>
          </td>
          <td>
            <p>&nbsp;Mike Cleary</p>
          </td>
        </tr><tr><td>
            <p>On Water Safety&nbsp;Officer</p>
          </td>
          <td>
            <p>&nbsp;Greg Adams</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;General Committee Member</p>
          </td>
          <td>
            <p>&nbsp;Kim Meikle</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;General Committee Member</p>
          </td>
          <td>
            <p>&nbsp;Andrew King</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;Formula 1 Representative</p>
          </td>
          <td>
            <p>&nbsp;Marty Chiminello</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;Formula 2 Representative</p>
          </td>
          <td>
            <p>&nbsp;Todd Leary</p>
          </td>
        </tr><tr><td>
            <p>Formula Opti&nbsp;Representative</p>
          </td>
          <td>
            <p>&nbsp;Grant Trask</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;Formula 3 Representative</p>
          </td>
          <td>
            <p>&nbsp;Brock Cohen</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;Formula 4 Representative&nbsp;</p>
          </td>
          <td>
            <p>&nbsp;Tracey Pugsley</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;Formula Futures Representative</p>
          </td>
          <td>
            <p>&nbsp;Martin Family Racing</p>
          </td>
        </tr><tr><td>
            <p>&nbsp;Club APBA Representative</p>
          </td>
          <td>
            <p>&nbsp;Rhys Coles &amp; Simon Troy</p>
          </td>
        </tr>
      -->
      </tbody></table></div>

  <div style="clear: both;">&nbsp;</div>
</div>
</div></div></div>  </div>

  <div class="group-right">
    <div data-edit-id="node/12/field_image/und/full" class="field field-name-field-image field-type-image field-label-hidden"><div class="field-items">


<?php

// check if the repeater field has rows of data
if( have_rows('about_side_images') ):

  // loop through the rows of data
    while ( have_rows('about_side_images') ) : the_row();

        // display a sub field value
        //the_sub_field('sub_field_name'); ?>
      <div class="field-item even about_side_img"><img alt="image" src="<?php the_sub_field('image'); ?>" typeof="foaf:Image"></div>

<?php

    endwhile;

else :

    // no rows found

endif;

?>


<!--
      <div class="field-item even"><img width="275" height="184" alt="Formula Powerboat Racing" src="<?php echo get_template_directory_uri(); ?>/images/About%201.jpg?itok=12OOHWzZ" typeof="foaf:Image"></div>

      <div class="field-item odd"><img width="275" height="183" alt="Australian Powerboats" src="<?php echo get_template_directory_uri(); ?>/images/About%202.jpg?itok=v2ueaa-D" typeof="foaf:Image"></div>

      <div class="field-item even"><img width="275" height="183" alt="Powerboat Strategies" src="<?php echo get_template_directory_uri(); ?>/images/About%203.jpg?itok=S8cqVP9u" typeof="foaf:Image"></div>

      <div class="field-item odd"><img width="275" height="183" alt="Powerboat Sponsers" src="<?php echo get_template_directory_uri(); ?>/images/About%205.jpg?itok=p0xRBXWx" typeof="foaf:Image"></div>

      <div class="field-item even"><img width="275" height="184" alt="Powerboat" src="<?php echo get_template_directory_uri(); ?>/images/About%204.jpg?itok=OXgR2JLk" typeof="foaf:Image"></div>

      <div class="field-item odd"><img width="275" height="183" alt="Buoys" src="<?php echo get_template_directory_uri(); ?>/images/About%208.jpg?itok=zYkL3VGA" typeof="foaf:Image"></div>

      <div class="field-item even"><img width="275" height="183" alt="Powerboat Drivers" src="<?php echo get_template_directory_uri(); ?>/images/About%206.jpg?itok=IH5RTtKZ" typeof="foaf:Image"></div>

      <div class="field-item odd"><img width="275" height="184" alt="Boats Racing" src="<?php echo get_template_directory_uri(); ?>/images/About%207.jpg?itok=ECTGsiJ7" typeof="foaf:Image"></div>
-->
    </div></div>  </div>

</div>

    </div>
  </div><!-- /block-inner -->
</div><!-- /block -->
    </div><!-- /content-inner -->
  </div><!-- /content -->
                                                      </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->
                                      </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div>

            
<?php get_footer(); ?>